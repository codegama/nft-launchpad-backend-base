<?php

/*
|--------------------------------------------------------------------------
| Application Constants
|--------------------------------------------------------------------------
|
| 
|
*/

if(!defined('COMMON_FILE_PATH')) define('COMMON_FILE_PATH', 'uploads/images/');

if(!defined('PROFILE_PATH_USER')) define('PROFILE_PATH_USER', 'uploads/users/');

if(!defined('PROFILE_PATH_CREATOR')) define('PROFILE_PATH_CREATOR', 'uploads/creators/');

if(!defined('PROFILE_PATH_ADMIN')) define('PROFILE_PATH_ADMIN', 'uploads/admins/');

if(!defined('FILE_PATH_SITE')) define('FILE_PATH_SITE', 'uploads/sites/');

if(!defined('SETTINGS_JSON')) define('SETTINGS_JSON', 'default-json/settings.json');

if(!defined('DOCUMENTS_PATH')) define('DOCUMENTS_PATH', 'uploads/documents/');

if(!defined('PROJECTS_PATH')) define('PROJECTS_PATH', 'uploads/projects/');

if(!defined('ARTWORK_PATH')) define('ARTWORK_PATH', 'uploads/art_works/');

if(!defined('CONTRACT_SAMPLE_PATH')) define('CONTRACT_SAMPLE_PATH', 'uploads/contract_sample/');

if(!defined('FILE_PATH_NFT')) define('FILE_PATH_NFT', 'uploads/nfts/');