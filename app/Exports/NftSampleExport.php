<?php

namespace App\Exports;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Http\Request;

use App\Models\Contract, App\Models\NftProperty;
  
class NftSampleExport implements FromView 
{



    public function __construct(Request $request)
    {
        $this->contract_unique_id = $request->contract_unique_id;
       
    }


    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View {

        $contract = Contract::firstWhere('unique_id', $this->contract_unique_id);

        $nft_properties = NftProperty::where('contract_id', $contract->id)->get();

        $data['properties'] = $nft_properties;

        $data['property_values'] = array("Black", "Cylinder", "Metal", "Single", "Rounded", "Long", "Red", "Genesis", "Nill", "Flat");

     return view('exports.nft_sample', [
            'data' => $data
        ]);


    }

}