<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use DB, Hash, Setting, Auth, Validator, Exception, Enveditor;

use \App\Helpers\Helper;

use App\Models\User, App\Models\Contract, App\Models\MergeImage, App\Models\MergeImageProperty;

use App\Models\NftProperty;

class AdminContractController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {

        $this->middleware('auth:admin');
       
        $this->paginate_count = 10;

        $this->timezone = Auth::guard('admin')->user()->timezone ?? 'Asia/Kolkata';
        
    }
    
    /**
     * @method contracts_index()
     *
     * @uses Used to list the contarcts
     *
     * @created Arun
     *
     * @updated   
     *
     * @param -
     *
     * @return List of pages   
     */

    public function contracts_index(Request $request) {

         $base_query = Contract::with('creator')->orderBy('created_at','desc');

        if($request->search_key) {

            $search_key = $request->search_key;

            $search_contract_ids =  $search_contract_ids = Contract::whereHas('creator', function($q) use ($search_key) {

                                                    return $q->Where('users.name','LIKE','%'.$search_key.'%');

                                                })->orWhere('contracts.contract_name', 'LIKE','%'.$search_key.'%')
                                                ->orWhere('contracts.symbol', 'LIKE','%'.$search_key.'%')
                                                ->pluck('id');

            $base_query = $base_query->whereIn('contracts.id',$search_contract_ids);

        }

        if($request->creator_id != '') {

            $base_query = $base_query->where('user_id',$request->creator_id);

            $creator = User::find($request->creator_id);

            if(!$creator) { 

                throw new Exception(tr('creator_not_found'), 101);
            }

        }

        if ($request->filled('status')) {
            
            $base_query = $base_query->where('publish_status', $request->status);
        }

        $contracts = $base_query->paginate($this->paginate_count);

        return view('admin.contracts.index')
                    ->with('page', 'contracts')
                    ->with('sub_page', 'contracts-view')
                    ->with('contracts', $contracts);
    
    }

    /**
     * @method contracts_create()
     *
     * @uses display create contract 
     *
     * @created Arun
     *
     * @updated    
     *
     * @param
     *
     * @return view page   
     *
     */
    public function contracts_create() {

        $contract = new Contract;

        $creators = User::Approved()->get();

        return view('admin.contracts.create')
                ->with('page', 'contracts')
                ->with('sub_page', 'contracts-create')
                ->with('creators', $creators)
                ->with('contract', $contract);
    }

    /**
     * @method contracts_edit()
     *
     * @uses To display and update contract details based on the contract id
     *
     * @created Vithya R
     *
     * @updated 
     *
     * @param object $request - contract Id
     * 
     * @return redirect view page 
     *
     */
    public function contracts_edit(Request $request) {

        try {

            $contract = Contract::find($request->contract_id);

            if(!$contract) {

                throw new Exception(tr('contract_not_found'), 101);
            }

            $creators = User::Approved()->get();

            $creators = selected($creators, $contract->user_id, 'id');

            return view('admin.contracts.edit')
                    ->with('page', 'contracts')
                    ->with('sub_page', 'contracts-view')
                    ->with('creators', $creators)
                    ->with('contract', $contract);

        } catch(Exception $e) {

            return redirect()->route('admin.contracts.index')->with('flash_error', $e->getMessage());

        }
    }

    /**
     * @method contracts_save()
     *
     * @uses To save the contract details of new/existing contract object based on details
     *
     * @created Arun
     *
     * @updated 
     *
     * @param
     *
     * @return index page    
     *
     */
    public function contracts_save(Request $request) {

        try {

            $timezone = Auth::guard('admin')->user()->timezone ?? 'Asia/Kolkata';

            DB::beginTransaction();

            $rules = [
                    'creator_id' => 'required|exists:users,id',
                    'contract_id' => 'nullable|exists:contracts,id',
                    'contract_name' => 'required|max:255',
                    'symbol' => 'required',
                    'total_supply' => 'required|numeric',
                    'pre_sale_start_date' => 'required|date|after:now',
                    'public_sale_start_date' => 'required|date|after:pre_sale_start_date',
                    'pre_sale_mint_fee' => 'required|numeric',
                    'public_sale_mint_fee' => 'required|numeric',
                    // 'total_nfts' => 'required|numeric|min:1',
                    'total_pre_sale_nfts' => 'required|numeric|lte:total_supply',
                    'total_public_sale_nfts' => 'required|numeric|lte:total_supply',
                    'sample_nft' => 'nullable|mimes:jpeg,jpg,png,gif,bmp,tiff',
                ]; 
                        
            Helper::custom_validator($request->all(), $rules);

            $contract = Contract::find($request->contract_id) ?? new Contract;

            $contract->user_id = $request->creator_id ?? 0;

            $contract->contract_name = $request->contract_name ?? $contract->contract_name;

            $contract->symbol = $request->symbol ?? $contract->symbol;

            $contract->total_supply = $request->total_supply ?? $contract->total_supply;

            $contract->description = $request->description ?? $contract->description;

            $contract->pre_sale_start_date = $request->pre_sale_start_date ? common_server_date($request->pre_sale_start_date, $timezone, "Y-m-d H:i:s") : $contract->pre_sale_start_date;

            $contract->public_sale_start_date = $request->public_sale_start_date ? common_server_date($request->public_sale_start_date, $timezone, "Y-m-d H:i:s") : $contract->public_sale_start_date;

            $contract->pre_sale_mint_fee = $request->pre_sale_mint_fee ?? $contract->pre_sale_mint_fee;

            $contract->public_sale_mint_fee = $request->public_sale_mint_fee ?? $contract->public_sale_mint_fee;

            $contract->total_nfts = $request->total_nfts ?? 0;

            $contract->total_pre_sale_nfts = $request->total_pre_sale_nfts ?? $contract->total_pre_sale_nfts;

            if (($request->total_supply - $request->total_pre_sale_nfts) < $request->total_public_sale_nfts) {
                
                throw new Exception(tr('total_public_sale_nft_less', ($request->total_supply - $request->total_pre_sale_nfts)), 101);
            }

            $contract->total_public_sale_nfts = ($contract->total_nfts - $contract->total_pre_sale_nfts) ?? 0;

            $contract->restrict_whitelisted_wallet = $request->restrict_whitelisted_wallet ? YES : NO;

            if($request->hasFile('sample_nft')){

                if($request->contract_id) {

                    Helper::storage_delete_file($request->sample_nft, CONTRACT_SAMPLE_PATH);
                }

                $contract->sample_nft = Helper::storage_upload_file($request->file('sample_nft'), CONTRACT_SAMPLE_PATH);

            }

            if($contract->save()) {

                $message = $request->contract_id ? tr('contract_updated_success') : tr('contract_created_success');

                DB::commit();
                
                return redirect()->route('admin.contracts.view', ['contract_id' => $contract->id] )->with('flash_success', $message);

            } 

            throw new Exception(tr('contract_save_failed'), 101);
                      
        } catch(Exception $e) {

            DB::rollback();

            return back()->withInput()->with('flash_error', $e->getMessage());

        }
    
    }

    /**
     * @method contracts_view()
     *
     * @uses view the contracts details based on contracts id
     *
     * @created Arun
     *
     * @updated 
     *
     * @param object $request
     * 
     * @return View page
     *
     */
    public function contracts_view(Request $request) {

        $contract = Contract::find($request->contract_id);

        if(!$contract) {
           
            return redirect()->route('admin.contracts.index')->with('flash_error',tr('contract_not_found'));

        }

        $contract->pre_sale_start_date = common_date($contract->pre_sale_start_date, ($this->timezone ?? "America/New_York"), "Y-m-d H:i:s");
            
        $contract->public_sale_start_date = common_date($contract->public_sale_start_date, ($this->timezone ?? "America/New_York"), "Y-m-d H:i:s");

        $current_date = strtotime(common_date(date("Y-m-d H:i:s"), ($this->timezone ?? "America/New_York"), "Y-m-d H:i:s"));

        $pre_sale_start_date = strtotime($contract->pre_sale_start_date);

        $public_sale_start_date = strtotime($contract->public_sale_start_date);

        $is_presale_started = $is_publicsale_started = NO;

        if ($current_date > $pre_sale_start_date) {
            
            $is_presale_started = $current_date < $public_sale_start_date ? YES : NO;

            $is_publicsale_started = $current_date > $public_sale_start_date ? YES : NO;
        }

        $contract->is_presale_started = $is_presale_started;

        $contract->is_publicsale_started = $is_publicsale_started;

        $contract->total_nfts = MergeImage::where('contract_id', $contract->id)->count() ?? 0;

        return view("admin.contracts.view")
                    ->with('page', 'contracts')
                    ->with('sub_page', 'contracts-view')
                    ->with('contract', $contract);
    }

    /**
     * @method contracts_status()
     *
     * @uses To update contract status as DECLINED/APPROVED based on contract id
     *
     * @created Arun
     *
     * @updated 
     *
     * @param - integer contract_id
     *
     * @return view page 
     */

    public function contracts_status(Request $request) {

        try {

            DB::beginTransaction();

            $contract = Contract::find($request->contract_id);

            if(!$contract) {

                throw new Exception(tr('contract_not_found'), 101);
                
            }

            $contract->status = $contract->status == DECLINED ? APPROVED : DECLINED;

            $contract->save();

            DB::commit();

            $message = $contract->status == DECLINED ? tr('contract_decline_success') : tr('contract_approve_success');

            return redirect()->back()->with('flash_success', $message);

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->back()->with('flash_error', $e->getMessage());

        }

    }

    /**
     * @method contracts_delete()
     *
     * @uses delete the contract details based on contract id
     *
     * @created Arun 
     *
     * @updated  
     *
     * @param object $request - contract Id
     * 
     * @return response of success/failure details with view page
     *
     */
    public function contracts_delete(Request $request) {

        try {

            DB::begintransaction();

            $contract = Contract::find($request->contract_id);
            
            if(!$contract) {

                throw new Exception(tr('contract_not_found'), 101);                
            }

            if($contract->delete()) {

                DB::commit();

                return redirect()->route('admin.contracts.index',['page'=>$request->page])->with('flash_success',tr('contract_deleted_success'));   

            } 
            
            throw new Exception(tr('contract_delete_failed'));
            
        } catch(Exception $e){

            DB::rollback();

            return redirect()->back()->with('flash_error', $e->getMessage());

        }       
         
    }

    /**
     * @method deploy_access_update()
     *
     * @uses To update contract deploy access as DEPLOY_ACCESS_GRANTED/DEPLOY_ACCESS_DECLINED based on contract id
     *
     * @created Arun
     *
     * @updated 
     *
     * @param - integer contract_id
     *
     * @return view page 
     */

    public function deploy_access_update(Request $request) {

        try {

            DB::beginTransaction();

            $rules = [
                    'deploye_access' => 'required|in:'.DEPLOY_ACCESS_GRANTED.','.DEPLOY_ACCESS_DECLINED,
                ]; 
                        
            Helper::custom_validator($request->all(), $rules);

            $contract = Contract::find($request->contract_id);

            if(!$contract) {

                throw new Exception(tr('contract_not_found'), 101);
                
            }

            $contract->deploye_access = $request->deploye_access ?? $contract->deploye_access;

            $contract->save();

            DB::commit();

            $message = $contract->deploye_access == DEPLOY_ACCESS_GRANTED ? tr('deploye_access_grant_success') : tr('deploye_access_decline_success');

            return redirect()->back()->with('flash_success', $message);

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->back()->with('flash_error', $e->getMessage());

        }

    }

    /**
     * @method nfts_index()
     *
     * @uses Used to list the Nft
     *
     * @created Arun
     *
     * @updated   
     *
     * @param -
     *
     * @return List of pages   
     */

    public function nfts_index(Request $request) {

        $base_query = MergeImage::orderBy('created_at', 'DESC');

        if($request->search_key) {

            $search_key = $request->search_key;

            $search_nft_ids =  $search_nft_ids = MergeImage::whereHas('contract', function($q) use ($search_key) {

                                                    return $q->Where('contracts.contract_name','LIKE','%'.$search_key.'%');

                                                })->orWhere('merge_images.file_name', 'LIKE','%'.$search_key.'%')
                                                ->pluck('id');

            $base_query = $base_query->whereIn('merge_images.id',$search_nft_ids);

        }

        if($request->filled('minted_user_id')) {

            $user = User::find($request->minted_user_id);

            if(!$user) {

                throw new Exception(tr('user_not_found'), 101);                
            }

            $base_query = $base_query->where('minted_wallet_address', $user->wallet_address);

        }

        if($request->filled('contract_id')) {

            $base_query = $base_query->where('contract_id', $request->contract_id);

        }

        if ($request->filled('mint_status')) {
            
            $base_query = $base_query->where('mint_status', $request->mint_status);
        }

        $contract = Contract::find($request->contract_id);

        $nfts = $base_query->paginate($this->paginate_count);

        return view('admin.nfts.index')
                    ->with('page', 'nfts')
                    ->with('sub_page', 'nfts-view')
                    ->with('contract', $contract)
                    ->with('nfts', $nfts);
    
    }

    /**
     * @method nfts_view()
     *
     * @uses view the Nft details based on Nft id
     *
     * @created Arun
     *
     * @updated 
     *
     * @param object $request
     * 
     * @return View page
     *
     */
    public function nfts_view(Request $request) {

        $nft = MergeImage::find($request->merge_image_id);

        if(!$nft) {
           
            return redirect()->route('admin.nfts.index')->with('flash_error',tr('nft_not_found'));

        }

        $merge_image_properties = MergeImageProperty::where('merge_image_id', $request->merge_image_id)->with('nftProperty')->get();

        $contract = Contract::find($nft->contract_id);

        $properties = NftProperty::where('contract_id', $contract->id)->get();

        $properties->map(function ($property, $key) use ($request, $nft) {

            $property->values = MergeImageProperty::where('nft_property_id', $property->id)->where('merge_image_id', $nft->id)->first();

            return $property;

        });

        return view("admin.nfts.view")
                    ->with('page', 'nfts')
                    ->with('sub_page', 'nfts-view')
                    ->with('contract', $contract)
                    ->with('properties', $properties)
                    ->with('nft', $nft);
    }

    /**
     * @method nfts_delete()
     *
     * @uses delete the Nft details based on Nft id
     *
     * @created Arun 
     *
     * @updated  
     *
     * @param object $request - Nft Id
     * 
     * @return response of success/failure details with view page
     *
     */
    public function nfts_delete(Request $request) {

        try {

            DB::begintransaction();

            $nft = MergeImage::find($request->merge_image_id);
            
            if(!$nft) {

                throw new Exception(tr('nft_not_found'), 101);                
            }

            if($nft->delete()) {

                DB::commit();

                return redirect()->route('admin.nfts.index',['page'=>$request->page])->with('flash_success',tr('nft_deleted_success'));   

            } 
            
            throw new Exception(tr('nft_delete_failed'));
            
        } catch(Exception $e){

            DB::rollback();

            return redirect()->back()->with('flash_error', $e->getMessage());

        }       
         
    }

}
