<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Redirect;

use DB, Hash, Setting, Auth, Validator, Exception, Enveditor;

use App\Helpers\Helper;

use App\Models\User, App\Models\CreatorApplication;

use Excel;

use App\Exports\CreatorsExport;

class AdminCreatorController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {

        $this->middleware('auth:admin');
       
        $this->paginate_count = 10;
        
    }

    /**
     * @method creators_index()
     *
     * @uses To list out creators details 
     *
     * @created Arun
     *
     * @updated 
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function creators_index(Request $request) {

        $base_query = User::Creator()->orderBy('created_at','desc');

        if($request->search_key) {

            $search_key = $request->search_key;

            $search_creator_ids = User::Creator()->where('users.email', 'LIKE','%'.$search_key.'%')
                            ->orWhere('users.name', 'LIKE','%'.$search_key.'%')
                            ->pluck('id');

            $base_query = $base_query->whereIn('users.id',$search_creator_ids);

        }

        if($request->filled('status')) {

            $base_query = $base_query->where('users.status', $request->status);
        }

        $creators = $base_query->paginate($this->paginate_count);

        return view('admin.creators.index')
                    ->with('page', "creators")
                    ->with('sub_page', "creators-view")
                    ->with('creators', $creators);
    
    }

    /**
     * @method creators_create()
     *
     * @uses To create creator details
     *
     * @created Arun
     *
     * @updated 
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function creators_create() {

        $creator = new User;

        $creator_applications = CreatorApplication::where('user_id', 0)->get();

        return view('admin.creators.create')
                    ->with('page', 'creators')
                    ->with('sub_page','creators-create')
                    ->with('creator', $creator)           
                    ->with('creator_applications', $creator_applications);           
   
    }

    /**
     * @method creators_save()
     *
     * @uses To save the creators details of new/existing creator object based on details
     *
     * @created Arun
     *
     * @updated 
     *
     * @param object request - creator Form Data
     *
     * @return success message
     *
     */
    public function creators_save(Request $request) {

        try {

            DB::begintransaction();

            $rules = [                
                'name' => 'required|max:191',
                'email' => $request->creator_id ? 'required|email|max:191|unique:users,email,'.$request->creator_id.',id' : 'required|email|max:191|unique:users,email,NULL,id',
                'password' => $request->creator_id ? "" : 'required|min:6|confirmed',
                'picture' => 'mimes:jpg,png,jpeg',
                'creator_id' => 'exists:users,id|nullable',
                'cover' => 'nullable|mimes:jpeg,bmp,png,jpg',
                'gender' => 'nullable|in:male,female,others',
            ];

            Helper::custom_validator($request->all(),$rules);

            $creator = User::Creator()->where('id', $request->creator_id)->first() ?? new User;

            if($creator->id) {

                $message = tr('creator_updated_success'); 

            } else {

                $creator->password = ($request->password) ? \Hash::make($request->password) : null;

                $message = tr('creator_created_success');

                $creator->token = Helper::generate_token();

                $creator->token_expiry = Helper::generate_token_expiry();
                
                $creator->login_by = $request->login_by ?: 'manual';

            }

            $creator->name = $request->name;

            $creator->wallet_address = $creator->unique_id ?? "U-".rand();

            $creator->email = $request->email ?? "";

            $creator->gender = $request->gender ?: "male";

            $creator->jwt_token = "";

            $creator->role = CREATOR;
            
            // Upload picture
            
            if($request->hasFile('picture')) {

                if($request->creator_id) {

                    Helper::storage_delete_file($creator->picture, PROFILE_PATH_CREATOR); 
                    // Delete the old pic
                }

                $creator->picture = Helper::storage_upload_file($request->file('picture'), PROFILE_PATH_CREATOR);
            }

            if($creator->save()) {

                if ($request->creator_application_id) {
                    
                    $creator_application = CreatorApplication::find($request->creator_application_id);

                    $creator_application->user_id = $creator->id;

                    $creator_application->save();

                }

                DB::commit(); 

                return redirect(route('admin.creators.view', ['creator_id' => $creator->id]))->with('flash_success', $message);

            } 

            throw new Exception(tr('creator_save_failed'));
            
        } catch(Exception $e){ 

            DB::rollback();

            return redirect()->back()->withInput()->with('flash_error', $e->getMessage());

        } 

    }

    /**
     * @method creators_view()
     *
     * @uses Display the specified creator details based on creator_id
     *
     * @created Arun 
     *
     * @updated 
     *
     * @param object $request - creator Id
     * 
     * @return View page
     *
     */
    public function creators_view(Request $request) {
       
        try {
      
            $creator = User::Creator()->where('id',$request->creator_id)->first();

            if(!$creator) { 

                throw new Exception(tr('creator_not_found'), 101);                
            }

            return view('admin.creators.view')
                        ->with('page', 'creators') 
                        ->with('sub_page','creators-view') 
                        ->with('creator' , $creator);
            
        } catch (Exception $e) {

            return redirect()->back()->with('flash_error', $e->getMessage());
        }
    
    }

    /**
     * @method creators_edit()
     *
     * @uses To display and update creator details based on the creator id
     *
     * @created Arun
     *
     * @updated 
     *
     * @param object $request - creator Id
     * 
     * @return redirect view page 
     *
     */
    public function creators_edit(Request $request) {

        try {

            $creator = User::Creator()->where('id',$request->creator_id)->first();

            if(!$creator) { 

                throw new Exception(tr('creator_not_found'), 101);
            }

            return view('admin.creators.edit')
                    ->with('page', 'creators')
                    ->with('sub_page', 'creators-view')
                    ->with('creator', $creator); 
            
        } catch(Exception $e) {

            return redirect()->route('admin.creators.index')->with('flash_error', $e->getMessage());
        }
    
    }

    /**
     * @method creators_status
     *
     * @uses To update creator status as DECLINED/APPROVED based on creators id
     *
     * @created Arun
     *
     * @updated 
     *
     * @param object $request - creator Id
     * 
     * @return response success/failure message
     *
     **/
    public function creators_status(Request $request) {

        try {

            DB::beginTransaction();

            $creator = User::Creator()->where('id', $request->creator_id)->first();

            if(!$creator) {

                throw new Exception(tr('creator_not_found'), 101);
                
            }

            $creator->status = $creator->status ? DECLINED : APPROVED ;

            if($creator->save()) {

                DB::commit();

                $message = $creator->status ? tr('creator_approve_success') : tr('creator_decline_success');

                return redirect()->back()->with('flash_success', $message);
            }
            
            throw new Exception(tr('creator_status_change_failed'));

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->route('admin.creators.index')->with('flash_error', $e->getMessage());

        }

    }

    /**
     * @method creators_delete()
     *
     * @uses delete the creator details based on creator id
     *
     * @created Arun 
     *
     * @updated  
     *
     * @param object $request - creator Id
     * 
     * @return response of success/failure details with view page
     *
     */
    public function creators_delete(Request $request) {

        try {

            DB::begintransaction();

            $creator = User::Creator()->where('id', $request->creator_id)->first();
            
            if(!$creator) {

                throw new Exception(tr('creator_not_found'), 101);                
            }

            if($creator->delete()) {

                DB::commit();


                return redirect()->route('admin.creators.index',['page'=>$request->page])->with('flash_success',tr('creator_deleted_success'));   

            } 
            
            throw new Exception(tr('creator_delete_failed'));
            
        } catch(Exception $e){

            DB::rollback();

            return redirect()->back()->with('flash_error', $e->getMessage());

        }       
         
    }

    /**
     * @method creators_bulk_action()
     * 
     * @uses To delete,approve,decline multiple creators
     *
     * @created Arun
     *
     * @updated 
     *
     * @param 
     *
     * @return success/failure message
     */
    public function creators_bulk_action(Request $request) {

        try {
            
            $action_name = $request->action_name ;

            $creator_ids = explode(',', $request->selected_creators);

            if (!$creator_ids && !$action_name) {

                throw new Exception(tr('creator_action_is_empty'));

            }

            DB::beginTransaction();

            if($action_name == 'bulk_delete'){

                $creator = User::Creator()->whereIn('id', $creator_ids)->delete();

                if ($creator) {

                    DB::commit();

                    return redirect()->back()->with('flash_success',tr('admin_creators_delete_success'));

                }

                throw new Exception(tr('creator_delete_failed'));

            }elseif($action_name == 'bulk_approve'){

                $creator = User::Creator()->whereIn('id', $creator_ids)->update(['status' => USER_APPROVED]);

                if ($creator) {

                    DB::commit();

                    return back()->with('flash_success',tr('admin_creators_approve_success'))->with('bulk_action','true');
                }

                throw new Exception(tr('creators_approve_failed'));  

            }elseif($action_name == 'bulk_decline'){
                
                $creator = User::Creator()->whereIn('id', $creator_ids)->update(['status' => USER_DECLINED]);

                if ($creator) {
                    
                    DB::commit();

                    return back()->with('flash_success',tr('admin_creators_decline_success'))->with('bulk_action','true');
                }

                throw new Exception(tr('creators_decline_failed')); 
            }

        } catch( Exception $e) {

            DB::rollback();

            return redirect()->back()->with('flash_error',$e->getMessage());
        }

    }

    /**
     * @method creators_applications_index()
     *
     * @uses To list out creators application details 
     *
     * @created Arun
     *
     * @updated 
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function creators_applications_index(Request $request) {

        $base_query = CreatorApplication::orderBy('created_at','desc');

        if($request->search_key) {

            $search_key = $request->search_key;

            $search_creator_ids = CreatorApplication::where('email', 'LIKE','%'.$search_key.'%')
                            ->orWhere('creator_name', 'LIKE','%'.$search_key.'%')
                            ->pluck('id');

            $base_query = $base_query->whereIn('id',$search_creator_ids);

        }

        if($request->status) {

            $base_query = $base_query->where('status', $request->status);
        }

        $creator_applications = $base_query->paginate($this->paginate_count);

        return view('admin.creators.applications.index')
                    ->with('page', "creator_applications")
                    ->with('creator_applications', $creator_applications);
    
    }

    /**
     * @method creator_application_view()
     *
     * @uses Display the specified creator application details based on creator_application_id
     *
     * @created Arun 
     *
     * @updated 
     *
     * @param object $request - creator application Id
     * 
     * @return View page
     *
     */
    public function creator_application_view(Request $request) {
       
        try {
      
            $creator_application = CreatorApplication::find($request->creator_application_id);

            if(!$creator_application) { 

                throw new Exception(tr('creator_application_not_found'), 101);                
            }

            return view('admin.creators.applications.view')
                        ->with('page', "creator_applications")
                        ->with('creator_application' , $creator_application);
            
        } catch (Exception $e) {

            return redirect()->back()->with('flash_error', $e->getMessage());
        }
    
    }

    /**
     * @method creator_application_delete()
     *
     * @uses delete the creator application details based on creator application id
     *
     * @created Arun 
     *
     * @updated  
     *
     * @param object $request - creator application Id
     * 
     * @return response of success/failure details with view page
     *
     */
    public function creator_application_delete(Request $request) {

        try {

            DB::begintransaction();

            $creator_application = CreatorApplication::find($request->creator_application_id);
            
            if(!$creator_application) {

                throw new Exception(tr('creator_application_not_found'), 101);                
            }

            if($creator_application->delete()) {

                DB::commit();

                return redirect()->route('admin.creators.applications.index',['page'=>$request->page])->with('flash_success',tr('creator_application_deleted_success'));   

            } 
            
            throw new Exception(tr('creator_application_delete_failed'));
            
        } catch(Exception $e){

            DB::rollback();

            return redirect()->back()->with('flash_error', $e->getMessage());

        }       
         
    }

    /**
     * @method creators_excel()
     *
     * @uses Export creators
     *
     * @created Arun 
     *
     * @updated  
     *
     * @param object $request - creator application Id
     * 
     * @return response of success/failure details with view page
     *
     */
    public function creators_excel(Request $request) {

        try{
            $file_format = '.xlsx';

            $filename = routefreestring(Setting::get('site_name'))."-".date('Y-m-d-h-i-s')."-".uniqid().$file_format;

            return Excel::download(new CreatorsExport($request), $filename);

        } catch(\Exception $e) {

            return redirect()->route('admin.users.index')->with('flash_error' , $e->getMessage());

        }

    }
}
