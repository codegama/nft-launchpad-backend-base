<?php

namespace App\Http\Controllers\Api\Creator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use DB, Log, Hash, Validator, Exception, Setting, App\Helpers\Helper;

use App\Models\User, App\Models\CreatorApplication;

use Tymon\JWTAuth\Facades\JWTAuth;

use Tymon\JWTAuth\Exceptions\TokenExpiredException;

use Tymon\JWTAuth\Exceptions\TokenInvalidException;

use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;

class AccountApiController extends Controller
{

    protected $loginUser, $skip, $take;

    public function __construct(Request $request) {

        Log::info(url()->current());

        Log::info("Request Data".print_r($request->all(), true));
        
        $this->loginUser = User::find($request->id);

        $this->skip = $request->skip ?: 0;

        $this->take = $request->take ?: (Setting::get('admin_take_count') ?: TAKE_COUNT);

        $this->timezone = $this->loginUser->timezone ?? "America/New_York";

    }

    /**
     * @method login()
     *
     * @uses Registered Creator can login using their email & password
     * 
     * @created Arun
     *
     * @updated 
     *
     * @param object $request - Creator Email & Password
     *
     * @return Json response with Creator details
     */
    public function login(Request $request) {

        try {
            
            DB::beginTransaction();

            $rules = [
                'email' => 'required|email',
                'password' => 'required',
            ];

            Helper::custom_validator($request->all(), $rules);

            $creator = User::where('email',$request->email)->where('role', CREATOR)->first();

            // Check the user details 

            if(!$creator) {

                throw new Exception(api_error(1011), 1011);

            }

            $credentials = request(['email', 'password']);

            if (! $token = auth('creator')->attempt($credentials)) {
                
                throw new Exception(api_error(186), 186);
            }

            $creator->jwt_token = $token ?? $creator->jwt_token;

            $creator->token = Helper::generate_token();

            $creator->token_expiry = Helper::generate_token_expiry();

            $creator->timezone = $request->timezone ?: $creator->timezone;

            $creator->save();
            
            DB::commit();

            return $this->sendResponse(api_success(101), 101, $data = $creator);

            // if(Hash::check($request->password, $creator->password)) {

            //     // Generate new tokens
                
            //     $creator->token = Helper::generate_token();

            //     $creator->token_expiry = Helper::generate_token_expiry();

            //     $creator->timezone = $request->timezone ?: $creator->timezone;

            //     $creator->save();

            //     $data = User::find($creator->id);

            //     DB::commit();
                
            //     // counter(); // For site analytics. Don't remove

            //     return $this->sendResponse(api_success(101), 101, $data);

            // } else {

            //     throw new Exception(api_error(186), 186);

            // }

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }
    
    }

    /**
     * @method register_application_save()
     *
     * @uses To save the register application
     *
     * @created Arun
     *
     * @updated 
     *
     * @param objecct $request : creator application details
     *
     * @return json response with creator application details
     */
    public function register_application_save(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = [
                    'creator_name' => 'required|max:255',
                    'agree_terms' => 'required|in:'.YES.','.NO,
                    'mint_time_period' => 'required|numeric|min:1',
                    'twitter_followers' => 'required|numeric',
                    'project_artwork' => 'required|max:255',
                    'is_collection_derivative' => 'required|in:'.YES.','.NO,
                    'discord_id' => 'required',
                    'email' => 'required|email|max:255',
                    'project_description' => 'required',
                    'project_long_term_goal' => 'required',
                    'about_team' => 'required',
                    'team_link_attachment' => 'nullable',
                    'partnerships' => 'required',
                    'partnerships_link_attachment' => 'nullable',
                    'investment' => 'required',
                    'investment_link_attachment' => 'nullable',
                    'whitepaper_roadmap' => 'required',
                    'whitepaper_roadmap_link_attachment' => 'nullable',
                    'artwork' => 'required',
                    'artwork_picture' => 'mimes:jpeg,jpg,bmp,png',
                    'twitter_link' => 'required',
                    'discord_link' => 'required',
                    'instagram_link' => 'nullable',
                    'website_link' => 'nullable',
                    'external_link' => 'nullable',
                    'mint_date' => 'required|date',
                    'items_count' => 'required|numeric',
                    'is_team_dox' => 'required|in:'.YES.','.NO,
                    'mint_amount' => 'numeric',
                    'questions' => 'nullable',
            ];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            // Validation end
            
            $creator_application = new CreatorApplication;

            $creator_application->creator_name = $request->creator_name;

            $creator_application->agree_terms = $request->agree_terms;

            $creator_application->mint_time_period = $request->mint_time_period;

            $creator_application->twitter_followers = $request->twitter_followers;

            $creator_application->project_artwork = $request->project_artwork;

            $creator_application->is_collection_derivative = $request->is_collection_derivative;

            $creator_application->discord_id = $request->discord_id;

            $creator_application->email = $request->email;

            $creator_application->project_description = $request->project_description;

            $creator_application->project_long_term_goal = $request->project_long_term_goal;

            $creator_application->about_team = $request->about_team;

            $creator_application->team_link_attachment = $request->team_link_attachment ?? "";

            $creator_application->partnerships = $request->partnerships;

            $creator_application->partnerships_link_attachment = $request->partnerships_link_attachment ?? "";

            $creator_application->investment = $request->investment;

            $creator_application->investment_link_attachment = $request->investment_link_attachment ?? "";

            $creator_application->whitepaper_roadmap = $request->whitepaper_roadmap;

            $creator_application->whitepaper_roadmap_link_attachment = $request->whitepaper_roadmap_link_attachment ?? "";

            $creator_application->artwork = $request->artwork;

            $creator_application->twitter_link = $request->twitter_link;

            $creator_application->discord_link = $request->discord_link;

            $creator_application->instagram_link = $request->instagram_link ?? "";

            $creator_application->website_link = $request->website_link ?? "";

            $creator_application->external_link = $request->external_link ?? "";

            $creator_application->mint_date = common_server_date($request->mint_date,"", "Y-m-d H:i:s");

            $creator_application->items_count = $request->items_count;

            $creator_application->is_team_dox = $request->is_team_dox;

            $creator_application->mint_amount = $request->mint_amount;

            $creator_application->questions = $request->questions;

            // Upload picture
            if($request->hasFile('artwork_picture') != "") {

                $creator_application->artwork_picture = Helper::storage_upload_file($request->file('artwork_picture'), ARTWORK_PATH);

            }

            if($creator_application->save()) {

                $data = CreatorApplication::find($creator_application->id);

                DB::commit();

                return $this->sendResponse($message = api_success(121), $success_code = 121, $data);

            } else {    

                throw new Exception(api_error(103), 103);
            }

        } catch (Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }
   
    }

    /** 
     * @method profile()
     *
     * @uses To display the Creator details based on Creator  id
     *
     * @created Arun
     *
     * @updated 
     *
     * @param object $request - Creator Id
     *
     * @return json response with Creator details
     */

    public function profile(Request $request) {

        try {

            $creator = User::where('id',$request->id)->with('creatorApplication')->first();

            return $this->sendResponse($message = "", $success_code = "", $creator);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());

        }
    
    }

    /**
     * @method update_profile()
     *
     * @uses To update the creator details
     *
     * @created Arun
     *
     * @updated 
     *
     * @param objecct $request : creator details
     *
     * @return json response with creator details
     */
    public function update_profile(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = [
                    'name' => 'required|max:255',
                    'email' => 'email|unique:creators,email,'.$request->id.'|max:255',
                    'mobile' => 'nullable|digits_between:6,13',
                    'picture' => 'mimes:jpeg,jpg,bmp,png',
                    'gender' => 'nullable|in:male,female,others',
                    'device_token' => '',
            ];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            // Validation end
            
            $creator = User::find($request->id);

            $creator->name = $request->name ?? $creator->name;
            
            if($request->has('email')) {

                $creator->email = $request->email;
            }

            // Upload picture
            if($request->hasFile('picture') != "") {

                Helper::storage_delete_file($creator->picture, PROFILE_PATH_CREATOR); // Delete the old pic

                $creator->picture = Helper::storage_upload_file($request->file('picture'), PROFILE_PATH_CREATOR);

            }

            if($creator->save()) {

                $data = User::find($creator->id);

                DB::commit();

                return $this->sendResponse($message = api_success(111), $success_code = 111, $data);

            } else {    

                throw new Exception(api_error(103), 103);
            }

        } catch (Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }
   
    }

    /**
     * @method change_password()
     *
     * @uses To change the password of the Creator
     *
     * @created Arun
     *
     * @updated 
     *
     * @param object $request - Password & confirm Password
     *
     * @return json response of the Creator
     */
    public function change_password(Request $request) {

        try {

            DB::beginTransaction();

            $rules = [
                'password' => 'required|confirmed|min:6',
                'old_password' => 'required|min:6',
            ]; 

            Helper::custom_validator($request->all(), $rules, $custom_errors =[]);

            $creator = User::find($request->id);

            if($creator->login_by != "manual") {

                throw new Exception(api_error(118), 118);
                
            }

            if(Hash::check($request->old_password,$creator->password)) {

                $creator->password = Hash::make($request->password);
                
                if($creator->save()) {

                    DB::commit();

                    return $this->sendResponse(api_success(104), $success_code = 104, $data = []);
                
                } else {

                    throw new Exception(api_error(103), 103);   
                }

            } else {

                throw new Exception(api_error(108) , 108);
            }

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }

    }

    /**
     * @method delete_account()
     * 
     * @uses Delete creator account based on creator id
     *
     * @created Arun
     *
     * @updated 
     *
     * @param object $request - Password and creator id
     *
     * @return json with boolean output
     */

    public function delete_account(Request $request) {

        try {

            DB::beginTransaction();

            $request->request->add([ 
                'login_by' => $this->loginUser ? $this->loginUser->login_by : "manual",
            ]);

            // Validation start

            $rules = ['password' => 'required_if:login_by,manual'];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            // Validation end

            $creator = User::find($request->id);

            // The password is not required when the creator is login from social. If manual means the password is required

            if($creator->login_by == 'manual') {

                if(!Hash::check($request->password, $creator->password)) {
         
                    throw new Exception(api_error(108), 108); 
                }
            
            }

            if($creator->delete()) {

                DB::commit();

                return $this->sendResponse(api_success(103), $success_code = 103, $data = []);

            } 

            throw new Exception(api_error(119), 119);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        }

    }

    /**
     * @method logout()
     *
     * @uses Logout the creator
     *
     * @created Arun
     *
     * @updated 
     *
     * @param 
     * 
     * @return
     */
    public function logout(Request $request) {

        // Invalidate the token
        try {

            $creator = User::find($request->id);

            JWTAuth::invalidate(JWTAuth::getToken());

            $creator->jwt_token = "";

            $creator->save();

            return $this->sendResponse(api_success(106), 106);

        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            throw new Exception(api_error(1013), 1013);
        }

    }

    /**
     * @method refresh_token_update()
     *
     * @uses Check and Refresh jwt token
     * 
     * @created Arun
     *
     * @updated 
     *
     * @param object $request - Creator Email & Password
     *
     * @return Json response with Creator details
     */
    public function refresh_token_update(Request $request) {

        try {

            $rules = [
                'id' => 'required|exists:users,id',
                'token' => 'required',
            ];

            Helper::custom_validator($request->all(), $rules);

            $creator = User::where('id',$request->id)->where('role', CREATOR)->first();

            // Check the user details 

            if(!$creator) {

                throw new Exception(api_error(1011), 1011);

            }

            $user = JWTAuth::parseToken()->authenticate();

            return $this->sendResponse(api_success(128), 128, $data = $creator);


        } catch(Exception $e) {

            if ($e instanceof TokenExpiredException) {

                try {

                    $new_token = JWTAuth::parseToken()->refresh();

                    $creator->jwt_token = $new_token;

                    $creator->save();
                    
                    return $this->sendResponse(api_success(127), 127, $data = $creator);

                } catch(Exception $e) {

                    $creator->jwt_token = "";

                    $creator->save();

                    return $this->sendError($e->getMessage(), $e->getCode(), $e->getMessage(), 401);
                }
            }
            elseif ($e instanceof TokenInvalidException){

                return $this->sendError($e->getMessage(), $e->getCode(), $e->getMessage(), 401);
            }
            elseif ($e instanceof TokenBlacklistedException){

                return $this->sendError($e->getMessage(), $e->getCode(), $e->getMessage(), 401);
            }
            else {

                return $this->sendError($e->getMessage(), $e->getCode(), $e->getMessage(), 401);
            }

        }
    
    }
}
