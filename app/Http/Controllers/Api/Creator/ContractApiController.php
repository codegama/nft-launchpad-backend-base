<?php

namespace App\Http\Controllers\Api\Creator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use DB, Log, Hash, Validator, Exception, Setting, App\Helpers\Helper;

use Carbon\Carbon;

use App\Imports\WhitelistedWalletImport;

use App\Models\User, App\Models\CreatorApplication, App\Models\Contract, App\Models\NftImage;

use App\Models\WhitelistedWallet;

use Excel;

use App\Repositories\ContractRepository as ContractRepo;

class ContractApiController extends Controller
{
    
    protected $loginUser, $skip, $take;

    public function __construct(Request $request) {

        Log::info(url()->current());

        Log::info("Request Data".print_r($request->all(), true));
        
        $this->loginUser = User::find($request->id);

        $this->skip = $request->skip ?: 0;

        $this->take = $request->take ?: (Setting::get('admin_take_count') ?: TAKE_COUNT);

        $this->timezone = $this->loginUser->timezone ?? "America/New_York";

    }

    /**
     * @method dashboard()
     *
     * @uses Get the dashboard
     *
     * @created Arun
     *
     * @updated 
     *
     * @param object $request
     *
     * @return json response with creator contract details
     */

    public function dashboard(Request $request) {

        try {

            $data['total_contracts'] = Contract::where('user_id', $request->id)->count();

            $data['deployed_contracts'] = Contract::where('user_id', $request->id)->where('is_deployed', YES)->count();

            $contract_ids = Contract::where('user_id', $request->id)->pluck('id');

            $data['total_nfts'] = NftImage::whereIn('contract_id', $contract_ids)->count();

            $date = Carbon::now()->subDays(7);

            $data['new_contracts'] = Contract::where('user_id', $request->id)->where('created_at', '>=', $date)->take(5)->get();

            return $this->sendResponse($message = "", $success_code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());

        }

    }

    /**
     * @method contracts_save()
     *
     * @uses To save the contract details based on creator  id
     *
     * @created Arun
     *
     * @updated 
     *
     * @param object $request - creator Id
     *
     * @return json response with creator details
     */

    public function contracts_save(Request $request) {

        try {

            DB::beginTransaction();

             // Validation start

            $rules = [
                    'contract_unique_id' => 'nullable|exists:contracts,unique_id,user_id,'.$request->id,
                    'contract_name' => 'required|max:255',
                    'symbol' => 'required',
                    'total_supply' => 'required|numeric',
                    'pre_sale_start_date' => 'required|date|after:now',
                    'public_sale_start_date' => 'required|date|after:pre_sale_start_date',
                    'pre_sale_mint_fee' => 'required|numeric',
                    'public_sale_mint_fee' => 'required|numeric',
                    // 'total_nfts' => 'required|numeric|min:1',
                    'total_pre_sale_nfts' => 'required|numeric|lte:total_supply',
                    'total_public_sale_nfts' => 'required|numeric|lte:total_supply',
                    'restrict_whitelisted_wallet' => 'required|in:'.YES.','.NO,
                    'whitelisted_wallets' => 'required_if:restrict_whitelisted_wallet,'.YES,
                    'sample_nft' => 'nullable|mimes:jpeg,jpg,png,gif,bmp,tiff',
                ];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            $creator = User::find($request->id);

            $contract = Contract::where('unique_id', $request->contract_unique_id)->first() ?? new Contract;

            $code = $contract->id ? 123 : 122;

            $contract->user_id = $request->id;

            $contract->contract_name = $request->contract_name ?? $contract->contract_name;

            $contract->symbol = $request->symbol ?? $contract->symbol;

            $contract->total_supply = $request->total_supply ?? $contract->total_supply;

            $contract->description = $request->description ?? $contract->description;

            $contract->pre_sale_start_date = $request->pre_sale_start_date ? common_server_date($request->pre_sale_start_date, $creator->timezone, "Y-m-d H:i:s") : $contract->pre_sale_start_date;

            $contract->public_sale_start_date = $request->public_sale_start_date ? common_server_date($request->public_sale_start_date, $creator->timezone, "Y-m-d H:i:s") : $contract->public_sale_start_date;

            $contract->pre_sale_mint_fee = $request->pre_sale_mint_fee ?? $contract->pre_sale_mint_fee;

            $contract->public_sale_mint_fee = $request->public_sale_mint_fee ?? $contract->public_sale_mint_fee;

            $contract->total_nfts = $request->total_nfts ?? 0;

            $contract->total_pre_sale_nfts = $request->total_pre_sale_nfts ?? $contract->total_pre_sale_nfts;

            $contract->total_public_sale_nfts = $request->total_public_sale_nfts ?? $contract->total_public_sale_nfts;

            $contract->restrict_whitelisted_wallet = $request->restrict_whitelisted_wallet ?? $contract->restrict_whitelisted_wallet;

            if($request->hasFile('sample_nft')){

                Helper::storage_delete_file($request->sample_nft, CONTRACT_SAMPLE_PATH);

                $contract->sample_nft = Helper::storage_upload_file($request->file('sample_nft'), CONTRACT_SAMPLE_PATH);

            }

            if ($contract->save()) {

                if ($request->restrict_whitelisted_wallet && $request->hasFile('whitelisted_wallets')) {
                
                    WhitelistedWallet::where('contract_id', $contract->id)->delete();

                    $request->request->add(['contract_id' => $contract->id]);

                    $import = new WhitelistedWalletImport($request);

                    Excel::import($import, request()->file('whitelisted_wallets'));
                }
                
                DB::commit();

                return $this->sendResponse(api_success($code), $success_code = $code, $contract);
            }

            throw new Exception(api_error(103), 103);
            
        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }

    }

    /**
     * @method contracts_list()
     *
     * @uses Get the list of contracts
     *
     * @created Arun
     *
     * @updated 
     *
     * @param object $request
     *
     * @return json response with creator contract details
     */

    public function contracts_list(Request $request) {

        try {

            $base_query = $total_query = Contract::where('user_id', $request->id)->orderBy('created_at', 'desc');

            if ($request->search_key) {
                
                $search_key = $request->search_key;

                $search_contract_ids = Contract::where('contract_name', 'LIKE','%'.$search_key.'%')
                                ->orWhere('symbol', 'LIKE','%'.$search_key.'%')
                                ->pluck('id');

                $base_query = $base_query->whereIn('id',$search_contract_ids);
            }

            $contracts = $base_query->skip($this->skip)->take($this->take)->get();

            $contracts = ContractRepo::contracts_list_response($contracts, $request);

            $data['contracts'] = $contracts;

            $data['total_contracts'] = Contract::where('user_id', $request->id)->count();

            return $this->sendResponse($message = "", $success_code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());

        }

    }

    /**
     * @method contracts_view()
     *
     * @uses Get the details of single contract by contract_unique_id
     *
     * @created Arun
     *
     * @updated 
     *
     * @param request contract_unique_id
     *
     * @return JSON Response
     */
    public function contracts_view(Request $request) {

        try {

            $rules = [
                'contract_unique_id' => 'required|exists:contracts,unique_id,user_id,'.$request->id,
            ];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            $contract = Contract::firstWhere('unique_id', $request->contract_unique_id);

            $contract = ContractRepo::contracts_single_response($contract, $request);

            return $this->sendResponse($message = '' , $code = '', $contract);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method contracts_delete()
     *
     * @uses Delete contract
     *
     * @created Arun
     *
     * @updated 
     *
     * @param request contract_unique_id
     *
     * @return JSON Response
     */
    public function contracts_delete(Request $request) {

        try {

            DB::beginTransaction();

            $rules = [
                'contract_unique_ids' => 'required',
            ];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            $contract_unique_ids = $request->contract_unique_ids;

            if (!is_array($contract_unique_ids)) {

                $contract_unique_ids = explode(',', $contract_unique_ids);
                
            }

            foreach ($contract_unique_ids as $key => $contract_unique_id) {

                $contract = Contract::where('unique_id',$contract_unique_id)->where('user_id', $request->id)->first();

                if(!$contract) {

                    throw new Exception(api_error(313), 313);                
                }

                $contract->delete();

            }

            DB::commit();

            return $this->sendResponse($message = api_success(124) , $code = 124, $data = []);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method update_deploy_access()
     *
     * @uses Change deploy access status DEPLOY_ACCESS_REQUESTED/DEPLOY_ACCESS_GRANTED/DEPLOY_ACCESS_DECLINED 
     *
     * @created Arun
     *
     * @updated 
     *
     * @param request contract_unique_id
     *
     * @return JSON Response
     */
    public function update_deploy_access(Request $request) {

        try {

            DB::beginTransaction();

            $rules = [
                'contract_unique_id' => 'required|exists:contracts,unique_id,user_id,'.$request->id,
                'deploye_access' => 'required|in:'.DEPLOY_ACCESS_REQUESTED.','.DEPLOY_ACCESS_NONE,
            ];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            $contract = Contract::firstWhere('unique_id', $request->contract_unique_id);

            $contract->deploye_access = $request->deploye_access ?? $contract->deploye_access;

            if ($contract->save()) {
                
                DB::commit();

                return $this->sendResponse($message = api_success(125) , $code = 125, $data = $contract);
            }

            throw new Exception(api_error(103), 103);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method deploy_contract_save()
     *
     * @uses Save deployed contract details
     *
     * @created Arun
     *
     * @updated 
     *
     * @param request contract_unique_id
     *
     * @return JSON Response
     */
    public function deploy_contract_save(Request $request) {

        try {

            DB::beginTransaction();

            $rules = [
                'contract_unique_id' => 'required|exists:contracts,unique_id,user_id,'.$request->id,
                'contract_address' => 'required',
                'wallet_address' => 'required',
                'transaction_hash' => 'required',
            ];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            $contract = Contract::firstWhere('unique_id', $request->contract_unique_id);

            $contract->contract_address = $request->contract_address ?? $contract->contract_address;
            
            $contract->wallet_address = $request->wallet_address ?? $contract->wallet_address;
            
            $contract->transaction_hash = $request->transaction_hash ?? $contract->transaction_hash;
            
            $contract->is_deployed = YES;

            $contract->publish_status = CONTACT_OPENED;

            if ($contract->save()) {
                
                DB::commit();

                return $this->sendResponse($message = api_success(126) , $code = 126, $data = $contract);
            }

            throw new Exception(api_error(103), 103);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method whitelisted_wallet_list()
     *
     * @uses Get the list of witelisted wallet address
     *
     * @created Arun
     *
     * @updated 
     *
     * @param object $request
     *
     * @return json response with creator contract details
     */

    public function whitelisted_wallet_list(Request $request) {

        try {

            $rules = [
                'contract_unique_id' => 'required|exists:contracts,unique_id,user_id,'.$request->id,
            ];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            $contract = Contract::firstWhere('unique_id', $request->contract_unique_id);

            $base_query = $total_query = WhitelistedWallet::where('contract_id', $contract->id)->orderBy('created_at', 'desc');

            if ($request->search_key) {
                
                $search_key = $request->search_key;

                $search_whitelisted_wallet_ids = WhitelistedWallet::where('wallet_address', 'LIKE','%'.$search_key.'%')
                                ->pluck('id');

                $base_query = $base_query->whereIn('id',$search_whitelisted_wallet_ids);
            }

            $whitelisted_wallets = $base_query->skip($this->skip)->take($this->take)->get();

            $data['whitelisted_wallets'] = $whitelisted_wallets;

            $data['total_whitelisted_wallets'] = $total_query->count();

            return $this->sendResponse($message = "", $success_code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());

        }

    }

    /**
     * @method whitelisted_wallet_save()
     *
     * @uses Save the list of witelisted wallet address
     *
     * @created Arun
     *
     * @updated 
     *
     * @param object $request
     *
     * @return json response with creator contract details
     */

    public function whitelisted_wallet_save(Request $request) {

        try {

            DB::beginTransaction();

            $rules = [
                'contract_unique_id' => 'required|exists:contracts,unique_id,user_id,'.$request->id,
                'restrict_whitelisted_wallet' => 'required|in:'.YES.','.NO,
                'whitelisted_wallets' => 'required_if:restrict_whitelisted_wallet,'.YES,
            ];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            $contract = Contract::firstWhere('unique_id', $request->contract_unique_id);

            $code = 134;

            $contract->restrict_whitelisted_wallet = $request->restrict_whitelisted_wallet ?? $contract->restrict_whitelisted_wallet;

            if ($contract->save()) {
                
                if ($request->restrict_whitelisted_wallet && $request->hasFile('whitelisted_wallets')) {
                
                    WhitelistedWallet::where('contract_id', $contract->id)->delete();

                    $request->request->add(['contract_id' => $contract->id]);

                    $import = new WhitelistedWalletImport($request);

                    Excel::import($import, request()->file('whitelisted_wallets'));

                    $code = 133;

                }

                DB::commit();

                return $this->sendResponse($message = api_success($code) , $code = $code, $data = $contract);
            }

            throw new Exception(api_error(192), 192);
            
        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }

    }

    /**
     * @method set_mint_fee_update()
     *
     * @uses Update the mint fee for the sale
     *
     * @created Arun
     *
     * @updated 
     *
     * @param request contract_unique_id
     *
     * @return JSON Response
     */
    public function set_mint_fee_update(Request $request) {

        try {

            DB::beginTransaction();

            $rules = [
                'contract_unique_id' => 'required|exists:contracts,unique_id,user_id,'.$request->id,
                'mint_fee_type' => 'required|in:'.MINT_TYPE_PRESALE.','.MINT_TYPE_PUBLICSALE,
            ];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            $contract = Contract::firstWhere('unique_id', $request->contract_unique_id);

            $code = 137;

            if (isset($request->is_release_limit)) {
                
                $contract->is_release_limit_set = YES;

            } else {

                $code = 136;

                if ($request->mint_fee_type == MINT_TYPE_PRESALE) {
                    
                    $contract->is_presale_fee_set = YES;
                }
                else {

                    $contract->is_publicsale_fee_set = YES;
                }
            }

            if ($contract->save()) {
                
                DB::commit();

                return $this->sendResponse($message = api_success($code) , $code = $code, $data = $contract);
            }

            throw new Exception(api_error(103), 103);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

}
