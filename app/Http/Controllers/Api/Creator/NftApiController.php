<?php

namespace App\Http\Controllers\Api\Creator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use DB, Log, Hash, Validator, Exception, Setting, Excel, File, App\Helpers\Helper;

use App\Models\User, App\Models\Contract, App\Models\NftImage, App\Models\NftProperty;

use App\Models\MergeImage, App\Models\MergeImageProperty;

use App\Imports\NftImport, App\Exports\NftSampleExport;

class NftApiController extends Controller
{
    
    protected $loginUser, $skip, $take;

    public function __construct(Request $request) {

        Log::info(url()->current());

        Log::info("Request Data".print_r($request->all(), true));
        
        $this->loginUser = User::find($request->id);

        $this->skip = $request->skip ?: 0;

        $this->take = $request->take ?: (Setting::get('admin_take_count') ?: TAKE_COUNT);

        $this->timezone = $this->loginUser->timezone ?? "America/New_York";

    }

    /**
     * @method nft_properties_save()
     *
     * @uses Nft Properties save with loggedin creators for a contract
     * 
     * @created Arun
     *
     * @updated 
     *
     * @param object $request
     *
     * @return Json response with creator details
     */
    public function nft_properties_save(Request $request) {

        try {
            
            DB::beginTransaction();

            $rules = [
                'contract_unique_id' => 'required|exists:contracts,unique_id,user_id,'.$request->id,
                'properties' => 'required',
            ];

            Helper::custom_validator($request->all(), $rules);

            $creator = User::find($request->id);

            if(!$creator) {

                throw new Exception(api_error(1002), 1002);

            }

            $contract = Contract::firstWhere('unique_id', $request->contract_unique_id);

            $properties = json_decode($request->properties);

            foreach ($properties as $key => $property) {

                if (isset($property->is_edited) && $property->is_edited) {

                    $nft_property = NftProperty::find($property->nft_property_id) ?? new NftProperty;

                    $old_nft_property = NftProperty::where('contract_id', $contract->id)->where('name', '=', $property->name)->first();

                    if ($old_nft_property && ($old_nft_property->id !=  $nft_property->id)) {
                        
                        throw new Exception(api_error(188), 188);
                    }

                    $nft_property->contract_id = $contract->id ?? 0;

                    $nft_property->name = $property->name ?? "";

                    if ($nft_property->save()) {
                        
                        foreach ($property->nft_images as $key => $property_image) {

                            if (isset($property_image->is_edited) && $property_image->is_edited) {
                                
                                $nft_image = NftImage::find($property_image->nft_image_id) ?? new NftImage;

                                $nft_image->contract_id = $contract->id ?? 0;

                                $nft_image->nft_property_id = $nft_property->id ?? 0;

                                if (!$nft_image->id) {
                                    
                                    $nft_image->picture = Helper::storage_upload_file_base($property_image->picture, FILE_PATH_NFT, rand());
                                }

                                $nft_image->value = $property_image->value ?? "";

                                $nft_image->is_used = $property_image->is_used ?? YES;

                                $nft_image->save();
                            }
                            
                        }
                    }
                    
                }
            }

            DB::commit();

            $contract = Contract::firstWhere('unique_id', $request->contract_unique_id);

            $nft_properties = NftProperty::where('contract_id', $contract->id)->with('nftImages')->get();

            $data['contract'] = $contract ?? [];

            $data['nft_properties'] = $nft_properties ?? [];

            return $this->sendResponse(api_success(135), 135, $data);
                   
        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }
    
    }

    /**
     * @method nft_properties_list()
     *
     * @uses Nft Properties list with loggedin users
     * 
     * @created Arun
     *
     * @updated 
     *
     * @param object $request
     *
     * @return Json response with user details
     */
    public function nft_properties_list(Request $request) {

        try {

            $rules = [
                'contract_unique_id' => 'required|exists:contracts,unique_id,user_id,'.$request->id,
            ];

            Helper::custom_validator($request->all(), $rules);

            $user = User::find($request->id);

            if(!$user) {

                throw new Exception(api_error(1002), 1002);

            }

            $contract = Contract::firstWhere('unique_id', $request->contract_unique_id);

            $nft_properties = NftProperty::where('contract_id', $contract->id)->with('nftImages')->get();

            $data['contract'] = $contract ?? [];

            $data['nft_properties'] = $nft_properties ?? [];

            return $this->sendResponse($message = "", $success_code = "", $data);
                   
        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());

        }
    
    }

    /**
     * @method nft_properties_delete()
     *
     * @uses delete the selected Nft Properties and repective images
     *
     * @created Arun
     *
     * @updated 
     *
     * @param integer contract_unique_id
     * 
     * @return JSON Response
     */

    public function nft_properties_delete(Request $request) {

        try {

            DB::beginTransaction();

            // validation start

            $rules = [
                'contract_unique_id' => 'required|exists:contracts,unique_id,user_id,'.$request->id,
                'nft_property_id' => 'required|integer|exists:nft_properties,id',
            ];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            $nft_property = NftProperty::find($request->nft_property_id);

            if ($nft_property->delete()) {
                
                DB::commit();

                $contract = Contract::firstWhere('unique_id', $request->contract_unique_id);

                $nft_properties = NftProperty::where('contract_id', $contract->id)->with('nftImages')->get();

                $data['contract'] = $contract ?? [];

                $data['nft_properties'] = $nft_properties ?? [];

                return $this->sendResponse(api_success(129), 129, $data = $data);
            }

            throw new Exception(api_error(191), 191);
                

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        }

    }

    /**
     * @method nft_merge_excel_import_save()
     *
     * @uses To import zip file and excel insert into the database
     *
     * @created Arun
     *
     * @updated 
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function nft_merge_excel_import_save(Request $request) {

        try {

            DB::beginTransaction();

            $rules = [ 

                'import_excel_file'  => 'required|mimes:xls,xlsx,csv',
                'import_zip_file'  => 'required|mimes:zip',
                'contract_unique_id' => 'required|exists:contracts,unique_id,user_id,'.$request->id,
            ];

            Helper::custom_validator($request->all(),$rules);

            $contact = Contract::firstWhere('unique_id', $request->contract_unique_id);

            $import = new NftImport($request);

            Excel::import($import, request()->file('import_excel_file'));
            
            $rows = (new NftImport($request))->toArray($request->import_excel_file);

            $row_count = $import->getRowCount();

            // $row_count = count($rows[0]);

            $file_name = [];

            for($i = 0; $i< $row_count; $i++){

                $file_name[] = $rows[0][$i]['name'];
            }
            
            //zip code

            Helper::custom_validator($request->all(),$rules);

            $zip_file_name = date('Y-m-d-h-i-s')."-images";

            $images_folder_name = date('Y-m-d-h-i-s')."-images-folder";

            $storage_path = 'storage/uploads/nfts/'.$request->id.'/'.$contact->unique_id;

            $request->import_zip_file->move(public_path($storage_path), $zip_file_name);
            
            $path = public_path($storage_path.'/'.$zip_file_name);

            $outputDirExtract = public_path($storage_path.'/'.$images_folder_name);

            File::makeDirectory($outputDirExtract, 0777, true, true);
           
            $zipFile = new \PhpZip\ZipFile();

            $zipFile
                ->openFile($path) 
                ->extractTo($outputDirExtract);

            $zipFile->close();

            unlink($path);

            $zip_files_count = count(File::files($outputDirExtract.'/nft_images'));
            
            \Log::info('zip_files_count'.$zip_files_count);

            \Log::info('row_count'.$row_count);

            if($row_count != $zip_files_count){

                File::deleteDirectory($outputDirExtract);

                throw new Exception(api_error(187), 187); 
            }

            $filesInFolder = File::files($outputDirExtract.'/nft_images');

            foreach($filesInFolder as $path) { 

                  $file = pathinfo($path);

                  $file['basename'];

                 if (!in_array($file['basename'], $file_name)) {

                    File::deleteDirectory($outputDirExtract);

                    throw new Exception(api_error(186), 186); 
                 }

                $merge_image = MergeImage::where('contract_id', $contact->id)->where('file_name', $file['basename'])->first();

                if($merge_image){

                    $file_path = $file['dirname'].'/'.$merge_image->file_name;

                    $merge_image->picture = asset(str_replace(public_path(), '', $file_path)); 

                    $merge_image->save();

                }
            }

            DB::commit(); 

            return $this->sendResponse($message = "Imported successfully", $code = "200", $data = $contact);

        }  catch (Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getLine());
              

        }
    
    }

    /**
     * @method nft_merge_images_list()
     *
     * @uses Nft Merged Images list with loggedin users
     * 
     * @created Arun
     *
     * @updated 
     *
     * @param object $request
     *
     * @return Json response with user details
     */
    public function nft_merge_images_list(Request $request) {

        try {

            $rules = [
                'contract_unique_id' => 'required|exists:contracts,unique_id,user_id,'.$request->id,
            ];

            Helper::custom_validator($request->all(), $rules);

            $contract = Contract::firstWhere('unique_id', $request->contract_unique_id);

            $base_query = $total_query = MergeImage::where('contract_id', $contract->id)->orderBy('created_at', 'DESC');

            if(isset($request->mint_status))
            {
                $base_query = $base_query->where('mint_status', $request->mint_status);
            }

            if(isset($request->status))
            {
                $base_query = $base_query->where('status', $request->status);
            }

            $merge_images = $base_query->skip($this->skip)->take($this->take)->get();

            $merge_images = $merge_images->map(function ($merge_image, $key) use ($request, $contract) {

                        $properties = NftProperty::where('contract_id', $contract->id)->get();

                        $properties->map(function ($property, $key) use ($request, $merge_image) {

                            $property->values = MergeImageProperty::where('nft_property_id', $property->id)->where('merge_image_id', $merge_image->id)->first();

                            return $property;

                        });

                        $merge_image->properties = $properties;

                        return $merge_image;

                    });

            $data['contract'] = $contract ?? [];

            $data['nft_images'] = $merge_images ?? [];

            $data['total'] = MergeImage::where('contract_id', $contract->id)->count() ?? 0;

            return $this->sendResponse($message = "", $success_code = "", $data);
                   
        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());

        }
    
    }

    /**
     * @method nft_merge_image_view()
     *
     * @uses Nft Merged Images View with loggedin users
     * 
     * @created Arun
     *
     * @updated 
     *
     * @param object $request
     *
     * @return Json response with user details
     */
    public function nft_merge_image_view(Request $request) {

        try {

            $rules = [
                'merge_image_unique_id' => 'required|exists:merge_images,unique_id',
            ];

            Helper::custom_validator($request->all(), $rules);

            $merge_image = MergeImage::where('unique_id', $request->merge_image_unique_id)->first();

            $merge_image_properties = MergeImageProperty::where('merge_image_id', $merge_image->id)->with('nftProperty')->get();

            $contract = Contract::find($merge_image->contract_id);

            $properties = NftProperty::where('contract_id', $contract->id)->get();

            $properties->map(function ($property, $key) use ($request, $merge_image) {

                $property->values = MergeImageProperty::where('nft_property_id', $property->id)->where('merge_image_id', $merge_image->id)->first();

                return $property;

            });

            $merge_image->properties = $properties;

            $data['contract'] = $contract ?? [];

            $data['nft_image'] = $merge_image ?? [];

            $data['merge_image_properties'] = $merge_image_properties ?? [];

            return $this->sendResponse($message = "", $success_code = "", $data);
                   
        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());

        }
    
    }

    /**
     * @method nft_merge_image_delete()
     *
     * @uses delete merge images
     *
     * @created Jeevan 
     *
     * @updated  
     *
     * @param object $request - merge_image_ids
     * 
     * @return response of success/failure details with view page
     *
     */
    public function nft_merge_image_delete(Request $request) {

        try {

            DB::begintransaction();

            $rules = [
                'merge_image_ids' => 'required',
                'contract_unique_id' => 'required|exists:contracts,unique_id,user_id,'.$request->id,
            ];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            $merge_image_ids = $request->merge_image_ids;

            if (!is_array($merge_image_ids)) {

                $merge_image_ids = explode(',', $merge_image_ids);
                
            }

            foreach ($merge_image_ids as $key => $merge_image_id) {

                $merge_image = MergeImage::find($merge_image_id);

                if(!$merge_image) {

                    throw new Exception(api_error(312), 312);                
                }

                $merge_image->delete();

            }

            DB::commit();

            $contract = Contract::firstWhere('unique_id', $request->contract_unique_id);

            $base_query = $total_query = MergeImage::where('contract_id', $contract->id)->orderBy('created_at', 'DESC');

            $merge_images = $base_query->skip($this->skip)->take($this->take)->get();

            $data['contract'] = $contract ?? [];

            $data['nft_images'] = $merge_images ?? [];

            $data['total'] = MergeImage::where('contract_id', $contract->id)->count() ?? 0;

        return $this->sendResponse(api_success(314), $success_code = 314, $data = $data);  
            
        } catch(Exception $e){

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }       
         
    }

    /**
     * @method nft_mint_status_update()
     *
     * @uses Nft Mint status update
     *
     * @created Arun
     *
     * @updated 
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function nft_mint_status_update(Request $request) {

        try {
            
            $rules = [
                'merge_image_unique_id' => 'required|exists:merge_images,unique_id',
                'mint_status' => 'required|in:'.MINT_INITIATED.','.MINT_COMPLETED,
            ];

            Helper::custom_validator($request->all(), $rules);

            DB::beginTransaction();

            $merge_image = MergeImage::where('unique_id', $request->merge_image_unique_id)->first();

            $merge_image->mint_status = $request->mint_status ?? $merge_image->mint_status;

            $merge_image->save();

            DB::commit();

            return $this->sendResponse(api_success(130), 130);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }
    
    }

    /**
     * @method nft_single_merge_images_save()
     *
     * @uses Nft Creation/Edit with loggedin users
     * 
     * @created Arun
     *
     * @updated 
     *
     * @param object $request
     *
     * @return Json response with user details
     */
    public function nft_single_merge_images_save(Request $request) {

        try {
            
            DB::beginTransaction();

            $rules = [
                'contract_unique_id' => 'required|exists:contracts,unique_id,user_id,'.$request->id,
                'merge_image_unique_id' => 'nullable|exists:merge_images,unique_id',
                'properties' => 'required',
                'picture' => 'nullable|mimes:jpeg,jpg,bmp,png',
            ];

            Helper::custom_validator($request->all(), $rules);

            $contract = Contract::firstWhere('unique_id', $request->contract_unique_id);

            $merge_image = MergeImage::firstWhere('unique_id', $request->merge_image_unique_id) ?? new MergeImage;

            $code = $merge_image->id ? 132 : 131;

            $merge_image->contract_id = $contract->id;

            $merge_image->file_name = $request->file_name ?? $merge_image->file_name;

            // Upload picture
            if($request->hasFile('picture') != "") {

                $file_path = FILE_PATH_NFT.$request->id.'/'.$contract->id.'/';

                $file_name = "";

                if ($merge_image->picture) {

                    $pathinfo = pathinfo($merge_image->picture);

                    $file_name = $pathinfo['filename'];

                    $file_path = strpos($pathinfo['dirname'], 'storage/');

                    $file_path = substr($pathinfo['dirname'], $file_path + strlen('storage/'), strlen($pathinfo['dirname'])).'/';

                }

                Helper::storage_delete_file($merge_image->picture, $file_path); // Delete the old pic

                $merge_image->picture = Helper::storage_upload_file($request->file('picture'), $file_path, $file_name);
            
            }

            if ($merge_image->save()) {

                $properties = json_decode($request->properties);

                foreach ($properties as $key => $property) {
                    
                    $merge_image_property = MergeImageProperty::firstWhere('id', $property->merge_image_property_id ?? 0) ?? new MergeImageProperty;

                    $merge_image_property->merge_image_id = $merge_image->id;

                    $merge_image_property->nft_property_id = $property->nft_property_id ?? 0;

                    $merge_image_property->value = $property->value ?? '';

                    $merge_image_property->save();
                }
            
            }

            DB::commit();

            return $this->sendResponse(api_success($code), $code, $merge_image);
                   
        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }
    
    }

    /**
     * @method export_sample_nft()
     *
     * @uses Nft Sample export
     * 
     * @created Arun
     *
     * @updated 
     *
     * @param object $request
     *
     * @return Json response with user details
     */
    public function export_sample_nft(Request $request) {

        try{

            $rules = [
                'contract_unique_id' => 'required|exists:contracts,unique_id,user_id,'.$request->id,
            ];

            Helper::custom_validator($request->all(), $rules);

            $contract = Contract::firstWhere('unique_id', $request->contract_unique_id);

            $file_format = '.xlsx';

            $filename = "Nft-Sample-".$this->loginUser->unique_id.$file_format;

            Excel::store(new NftSampleExport($request), $filename, "public");

            $data['file_url'] = asset(\Storage::url($filename));

            return $this->sendResponse($message = "", $success_code = "", $data = $data);

        } catch(\Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());

        }

    }

}
