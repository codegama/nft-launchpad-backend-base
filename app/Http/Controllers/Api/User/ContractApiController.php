<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use DB, Hash, Setting, Validator, Exception, Enveditor, Log;

use App\Helpers\Helper;

use App\Models\User, App\Models\Contract, App\Models\NftImage, App\Models\MergeImage, App\Models\NftProperty;

use App\Models\MergeImageProperty, App\Models\WhitelistedWallet;

use App\Repositories\ContractRepository as ContractRepo;

class ContractApiController extends Controller
{
    
    protected $loginUser;

    protected $skip, $take;

    public function __construct(Request $request) {

        Log::info(url()->current());

        Log::info("Request Data".print_r($request->all(), true));
        
        $this->loginUser = User::find($request->id);

        $this->skip = $request->skip ?: 0;

        $this->take = $request->take ?: (Setting::get('admin_take_count') ?: TAKE_COUNT);

        $this->timezone = $this->loginUser->timezone ?? "America/New_York";

    }

    /**
     * @method contracts_list()
     *
     * @uses Get the list of contracts
     *
     * @created Arun
     *
     * @updated 
     *
     * @param object $request
     *
     * @return json response with creator contract details
     */

    public function contracts_list(Request $request) {

        try {

            $opened_contracts = Contract::Opened()->orderBy('contracts.updated_at', 'desc')->skip($this->skip)->take($this->take)->get();

            $opened_contracts = ContractRepo::contracts_list_response($opened_contracts, $request);

            $upcoming_contracts = Contract::Upcoming()->orderBy('contracts.updated_at', 'desc')->skip($this->skip)->take($this->take)->get();

            $upcoming_contracts = ContractRepo::contracts_list_response($upcoming_contracts, $request);

            $closed_contracts = Contract::Closed()->orderBy('contracts.updated_at', 'desc')->skip($this->skip)->take($this->take)->get();

            $closed_contracts = ContractRepo::contracts_list_response($closed_contracts, $request);

            $data['opened_contracts'] = $opened_contracts;

            $data['upcoming_contracts'] = $upcoming_contracts;

            $data['closed_contracts'] = $closed_contracts;

            return $this->sendResponse($message = "", $success_code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());

        }

    }

    /**
     * @method contracts_view()
     *
     * @uses Get the details of single contract by contract_unique_id
     *
     * @created Arun
     *
     * @updated 
     *
     * @param request contract_unique_id
     *
     * @return JSON Response
     */
    public function contracts_view(Request $request) {

        try {

            $rules = [
                'contract_unique_id' => 'required|exists:contracts,unique_id',
            ];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            $contract = Contract::firstWhere('unique_id', $request->contract_unique_id);

            $contract = ContractRepo::contracts_single_response($contract, $request);

            $nfts = NftImage::where('contract_id', $contract->id)->take(5)->get();

            $data['contract'] = $contract;

            $data['nfts'] = $nfts;

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method mint_nft_images()
     *
     * @uses Get the nft image to mint
     *
     * @created Arun
     *
     * @updated 
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function mint_nft_images(Request $request) {

        try {

            DB::beginTransaction();

            $rules = [
                'contract_unique_id' => 'required|exists:contracts,unique_id',
                'minted_wallet_address' => 'required',
                'mint_type' => 'required|in:'.MINT_TYPE_PRESALE.','.MINT_TYPE_PUBLICSALE
            ];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            $contract = Contract::firstWhere('unique_id', $request->contract_unique_id);

            $whitelisted_wallet = WhitelistedWallet::where('contract_id', $contract->id)
                                        ->where('wallet_address', $request->minted_wallet_address)
                                        ->first();

            if ($contract->restrict_whitelisted_wallet && !$whitelisted_wallet) {
                
                throw new Exception(api_error(196), 196);
                
            }
            
            $merge_image = MergeImage::where('contract_id', $contract->id)
                            ->where('mint_status', MINT_PENDING)
                            ->orderBy('merge_images.id', 'asc')
                            ->first();

            if(!$merge_image) {

                throw new Exception(api_error(193), 193);
            }

            $presale_minted = MergeImage::where('contract_id', $contract->id)
                            ->where('mint_status', '!=',MINT_PENDING)
                            ->where('mint_type', MINT_TYPE_PRESALE)
                            ->count();

            if ($presale_minted >= $contract->total_pre_sale_nfts) {
                
                throw new Exception(api_error(195), 195);
            }
            
            $merge_image->mint_status = MINT_INITIATED;

            $merge_image->mint_type = $request->mint_type ?? MINT_TYPE_PRESALE;

            $merge_image->save();

            DB::commit();

            $nft_properties = NftProperty::where('contract_id', $contract->id)->get();

            $nft_properties = $nft_properties->map(function ($nft_property, $key) use ($request, $merge_image) {

                        $merge_image_property = MergeImageProperty::where('merge_image_id', $merge_image->id)
                                                ->where('nft_property_id', $nft_property->id)
                                                ->first() ?? emptyObject();

                        $nft_property->merge_image_property = $merge_image_property;
                        
                        return $nft_property;
                    });

            $data['contract'] = $contract ?? emptyObject();

            $data['merge_image'] = $merge_image ?? emptyObject();

            $data['nft_properties'] = $nft_properties ?? [];

            return $this->sendResponse($message = "", $code = 200, $data = $data);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }
    
    }

    /**
     * @method nft_image_mint_update()
     *
     * @uses Update the mint status with minted wallet address
     *
     * @created Arun
     *
     * @updated 
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function nft_image_mint_update(Request $request) {

        try {

            DB::beginTransaction();

            $rules = [
                'merge_image_id' => 'required|exists:merge_images,id',
                'minted_wallet_address' => 'required',
            ];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);
            
            $merge_image = MergeImage::where('id', $request->merge_image_id)
                            ->where('mint_status', MINT_INITIATED)
                            ->first();

            if(!$merge_image) {

                throw new Exception(api_error(194), 194);
            }

            $merge_image->mint_status = MINT_COMPLETED;

            $merge_image->minted_wallet_address = $request->minted_wallet_address ?? "";

            if ($merge_image->save()) {
                
                $contract = Contract::firstWhere('id', $merge_image->contract_id);

                if ($contract->is_all_nfts_minted) {
                    
                    $contract->publish_status = CONTACT_COMPLETED;

                    $contract->save();
                }

                DB::commit();

            }

            return $this->sendResponse($message = "", $code = 200, $data = $merge_image);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }
    
    }

    /**
     * @method minted_contracts()
     *
     * @uses 
     *
     * @created Arun
     *
     * @updated 
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function minted_contracts(Request $request) {

        try {

            $user = User::find($request->id);

            $minted_contract_ids = MergeImage::where('minted_wallet_address',$user->wallet_address)
                                    ->where('mint_status', MINT_COMPLETED)
                                    ->pluck('contract_id');

            $minted_contracts = Contract::whereIn('id', $minted_contract_ids)->skip($this->skip)->take($this->take)->get();

            $minted_contracts->map(function ($minted_contract, $key) use ($request, $user) {

                $minted_contract->minted_items_count = MergeImage::where('contract_id', $minted_contract->id)
                                                        ->where('minted_wallet_address',$user->wallet_address)
                                                        ->where('mint_status', MINT_COMPLETED)
                                                        ->count() ?? 0;

                return $minted_contract;
            });

            $data['total'] = Contract::whereIn('id', $minted_contract_ids)->count() ?? 0;

            $data['minted_contracts'] = $minted_contracts ?? emptyObject();

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method nft_mint_status_update()
     *
     * @uses Update the mint status 
     *
     * @created Arun
     *
     * @updated 
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function nft_mint_status_update(Request $request) {

        try {

            DB::beginTransaction();

            $rules = [
                'merge_image_id' => 'required|exists:merge_images,id',
                'mint_status' => 'required|in:'.MINT_PENDING.','.MINT_INITIATED.','.MINT_COMPLETED,
            ];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);
            
            $merge_image = MergeImage::where('id', $request->merge_image_id)->first();

            if(!$merge_image) {

                throw new Exception(api_error(194), 194);
            }

            $merge_image->mint_status = $request->mint_status ?? MINT_PENDING;

            if ($merge_image->save()) {
                
                $contract = Contract::firstWhere('id', $merge_image->contract_id);

                if ($contract->is_all_nfts_minted) {
                    
                    $contract->publish_status = CONTACT_COMPLETED;

                    $contract->save();
                }

                DB::commit();

            }

            return $this->sendResponse($message = "", $code = 200, $data = $merge_image);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }
    
    }

}
