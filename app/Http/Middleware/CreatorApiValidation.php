<?php

namespace App\Http\Middleware;

use Closure;

use Illuminate\Http\Request;

use App\Helpers\Helper;

use Validator, Log, DB, Setting;

use App\Models\User;

class CreatorApiValidation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Log::info("Creator API - Creator ID".$request->id);

        //JWT token from header
        $token = $request->bearerToken();

        if (!$token) {
            
            $response = array('success' => false, 'error' => api_error(1012) , 'error_code' => 1012 );

            return response()->json($response,401);
        }

        $request->request->add([ 'token' => $token, ]);

        $validator = Validator::make(
                $request->all(),
                array(
                        'token' => 'required|min:5',
                        'id' => 'required|integer|exists:users,id'
                ),[
                    'exists' => api_error(1006),
                    'id' => api_error(1005)
                ]);

        if ($validator->fails()) {

            $error = implode(',', $validator->messages()->all());

            $response = array('success' => false, 'error' => $error , 'error_code' => 1006 );

            return response()->json($response,200);

        } else {

            $token = $request->token;

            $creator_id = $request->id;

            if (!Helper::is_jwt_token_valid(CREATOR, $creator_id, $token, $error)) {

                $response = response()->json($error, 401);
                
                return $response;

            } else {

                $creator = User::Creator()->where('id', $request->id)->first();

                if(!$creator) {
                    
                    $response = array('success' => false, 'error' => api_error(1006) , 'error_code' => 1006 );

                    return response()->json($response,200);

                }

                if(in_array($creator->status , [CREATOR_DECLINED , CREATOR_PENDING])) {
                    
                    $response = array('success' => false , 'error' => api_error(1000) , 'error_code' => 1000);

                    return response()->json($response, 200);
               
                }

            }
       
        }

        return $next($request);
    }
}
