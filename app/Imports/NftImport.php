<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Models\NftImage, App\Models\MergeImage, App\Models\Contract, App\Models\MergeImageProperty;
use App\Models\NftProperty;
use App\Helpers\Helper, App\Helpers\EnvEditorHelper;

class NftImport implements ToCollection, WithChunkReading, WithStartRow, SkipsOnError, WithHeadingRow
{
    use Importable;

    public $rows = 0;

    public function __construct($request)
    {
        $this->contract_unique_id = $request->contract_unique_id;
       
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }

    public function onError(\Throwable $e)
    {
        return $this->sendError($e->getMessage());
    }

    public function chunkSize(): int
    {
        return 1000;
    }
    
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        try {

            $rules = [ 

                '*.name'  => 'required',
            ];
            $custom_errors = [

                'required' => 'In row :attribute field is required.',
                'unique' => 'In row :attribute file already exits'

            ];
           
            Helper::custom_validator($collection->toArray(),$rules,$custom_errors);

            $contact = Contract::firstWhere('unique_id', $this->contract_unique_id);

            $merge_image_data = [];

            foreach ($collection as $row) {

                ++$this->rows;

                $data = MergeImage::create([
                    'file_name' => $row['name'],
                    'contract_id' => $contact->id ?? 0,
                ]);

                foreach($row as $key => $value){

                    if ($key != 'name' && $key != '') {
                        
                        $nft_property = NftProperty::where('contract_id', $contact->id)->where('name', '=', $key)->first();

                        if (!$nft_property) {
                            \Log::info('row_count'.$key);
                            
                            throw new \Exception(api_error(190), 190);
                        }

                        MergeImageProperty::create([
                            'merge_image_id' => $data->id,
                            'nft_property_id' => $nft_property->id,
                            'value' => $value,
                            ]);
                    }

                }
            }

        } catch (Exception $e) {

            return $e;
        }
    }

    public function getRowCount(): int
    {
        return $this->rows;
    }

}
