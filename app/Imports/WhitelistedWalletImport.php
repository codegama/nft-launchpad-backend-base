<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

use App\Models\WhitelistedWallet, App\Models\Project;

use App\Helpers\Helper;

class WhitelistedWalletImport implements ToCollection, WithChunkReading, WithStartRow, SkipsOnError, WithHeadingRow
{
    use Importable;

    public $rows = 0;

    public function __construct($request)
    {
        $this->contract_id = $request->contract_id;

        $this->user_id = $request->id;
       
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }

    public function onError(\Throwable $e)
    {
        return $this->sendError($e->getMessage());
    }

    public function chunkSize(): int
    {
        return 1000;
    }
    
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        try {

            $rules = [ 

                '*.wallet_address'  => 'required',
            ];
            $custom_errors = [

                'required' => 'In row :attribute field is required.',
                'unique' => 'In row :attribute file already exits'

            ];

            \Log::info('row_count'.$collection);
           
            Helper::custom_validator($collection->toArray(),$rules,$custom_errors);

            \Log::info('row_count'.$collection);

            foreach ($collection as $row) {

                ++$this->rows;

                $data = WhitelistedWallet::create([
                    'wallet_address' => $row['wallet_address'],
                    'contract_id' => $this->contract_id ?? 0,
                    'user_id' => $this->user_id ?? 0,
                ]);

            }

        } catch (Exception $e) {

            return $e;
        }
    }

    public function getRowCount(): int
    {
        return $this->rows;
    }

}
