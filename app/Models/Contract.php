<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\MergeImage;

class Contract extends Model
{
    use HasFactory;

    protected $hidden = ['id', 'unique_id'];

    protected $appends = ['contract_id', 'contract_unique_id', 'total_minted_nfts', 'is_all_nfts_minted'];
    
    public function getContractIdAttribute() {

        return $this->id;
    }

    public function getContractUniqueIdAttribute() {

        return $this->unique_id;
    }

    public function getTotalMintedNftsAttribute() {

        $total_minted_nfts = MergeImage::where('contract_id', $this->id)->where('mint_status', MINT_COMPLETED)->count();

        return $total_minted_nfts ?? 0;
    }

    public function getIsAllNftsMintedAttribute() {

        $is_all_nfts_minted = MergeImage::where('contract_id', $this->id)->where('mint_status', MINT_PENDING)->count();

        return $is_all_nfts_minted ? NO : YES;
    }

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUpcoming($query) {

        $query->where('contracts.publish_status', CONTACT_UPCOMING);

        return $query;

    }

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeClosed($query) {

        $query->where('contracts.publish_status', CONTACT_COMPLETED);

        return $query;

    }

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOpened($query) {

        $query->where('contracts.publish_status', CONTACT_OPENED);

        return $query;

    }

    public function creator() {

       return $this->belongsTo(User::class, 'user_id');
    }

    public function mergeImages() {

        return $this->hasMany(MergeImage::class);
    }

    public function nftProperties() {

        return $this->hasMany(NftProperty::class);
    }

    public static function boot() {

        parent::boot();

        static::creating(function ($model) {

            $model->attributes['unique_id'] = "C"."-".uniqid();

        });

        static::created(function($model) {

            $model->attributes['unique_id'] = "C"."-".$model->attributes['id']."-".uniqid();

            $model->save();
        
        });

        static::deleting(function ($model){

            foreach ($model->mergeImages as $key => $merge_image) {

                $merge_image->delete();
            }

            foreach ($model->nftProperties as $key => $nft_property) {

                $nft_property->delete();
            }

        });

    }
}
