<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;

use Setting, DB, Log;

use App\Helpers\Helper;

class Creator extends Authenticatable
{
    use HasFactory, Notifiable;

    // protected $hidden = ['id', 'unique_id'];

    protected $appends = ['creator_id', 'creator_unique_id'];
    
    public function getCreatorIdAttribute() {

        return $this->id;
    }

    public function getCreatorUniqueIdAttribute() {

        return $this->unique_id;
    }

    public function getRoleAttribute() {

        return CREATOR;
    }

    /**
     * Scope a query to only include active creators.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeApproved($query) {

        $query->where('creators.status', CREATOR_APPROVED);

        return $query;

    }

    public function creatorApplication() {

        return $this->hasOne(CreatorApplication::class, 'creator_id');

    }

    public static function boot() {

        parent::boot();

        static::creating(function ($model) {

            $model->attributes['unique_id'] = "C"."-".uniqid();

            $model->attributes['token'] = Helper::generate_token();

            $model->attributes['token_expiry'] = Helper::generate_token_expiry();

            $model->attributes['status'] = USER_APPROVED;

        });

        static::created(function($model) {

            $model->attributes['unique_id'] = "C"."-".$model->attributes['id']."-".uniqid();

            $model->save();
        
        });

    }
}
