<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MergeImage extends Model
{
    use HasFactory;

    protected $hidden = ['id', 'unique_id'];

    protected $appends = ['merge_image_id', 'merge_image_unique_id', 'mint_status_formatted'];

    protected $fillable = [ 'contract_id', 'file_name', 'picture'];
    
    public function getMergeImageIdAttribute() {

        return $this->id;
    }

    public function getMergeImageUniqueIdAttribute() {

        return $this->unique_id;
    }

    public function getMintStatusFormattedAttribute() {

        return mint_status_formatted($this->mint_status);
    }


    public function contract() {

        return $this->belongsTo(Contract::class,'contract_id');
    }

    public function mergeImageProperties() {

        return $this->hasMany(MergeImageProperty::class);
    }

    public static function boot() {

        parent::boot();

        static::creating(function ($model) {

            $model->attributes['unique_id'] = "MI"."-".uniqid();

        });

        static::created(function($model) {

            $model->attributes['unique_id'] = "MI"."-".$model->attributes['id']."-".uniqid();

            $model->save();
        
        });

        static::deleting(function ($model){

            $model->mergeImageProperties()->delete();

        });

    }
}
