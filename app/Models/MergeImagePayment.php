<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MergeImagePayment extends Model
{
    use HasFactory;

    protected $hidden = ['id', 'unique_id'];

    protected $appends = ['merge_image_payment_id', 'merge_image_payment_unique_id'];
    
    public function getMergeImagePaymentIdAttribute() {

        return $this->id;
    }

    public function getMergeImagePaymentUniqueIdAttribute() {

        return $this->unique_id;
    }

    public static function boot() {

        parent::boot();

        static::creating(function ($model) {

            $model->attributes['unique_id'] = "MIP"."-".uniqid();

        });

        static::created(function($model) {

            $model->attributes['unique_id'] = "MIP"."-".$model->attributes['id']."-".uniqid();

            $model->save();
        
        });

    }
}
