<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MergeImageProperty extends Model
{
    use HasFactory;

    protected $hidden = ['id', 'unique_id'];

    protected $appends = ['merge_image_property_id', 'merge_image_property_unique_id'];

    protected $fillable = ['merge_image_id','nft_property_id','value'];
    
    public function getMergeImagePropertyIdAttribute() {

        return $this->id;
    }

    public function getMergeImagePropertyUniqueIdAttribute() {

        return $this->unique_id;
    }

    public function nftProperty() {

        return $this->belongsTo(NftProperty::class,'nft_property_id');
    }

    public static function boot() {

        parent::boot();

        static::creating(function ($model) {

            $model->attributes['unique_id'] = "MIP"."-".uniqid();

        });

        static::created(function($model) {

            $model->attributes['unique_id'] = "MIP"."-".$model->attributes['id']."-".uniqid();

            $model->save();
        
        });

    }
}
