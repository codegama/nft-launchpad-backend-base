<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NftImage extends Model
{
    use HasFactory;

    protected $appends = ['nft_image_id', 'nft_image_unique_id', 'is_edited'];

    public function getNftImageIdAttribute() {

        return $this->id;
    }

    public function getNftImageUniqueIdAttribute() {

        return $this->unique_id;
    }

    public function getIsEditedAttribute() {

        return NO;
    }

    public function contract() {
        
        return $this->belongsTo(Contract::class, 'contract_id');
    }

    public function nftProperty() {

        return $this->belongsTo(NftProperty::class, 'nft_property_id');
    }

    public static function boot() {

        parent::boot();

        static::creating(function ($model) {

            $model->attributes['unique_id'] = 'NFT-I'."-".uniqid();
        });

        static::created(function($model) {

            $model->attributes['unique_id'] = 'NFT-I'."-".$model->attributes['id']."-".uniqid();

            $model->save();
        
        });

        static::deleting(function ($model) {

        });

    }
}
