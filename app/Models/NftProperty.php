<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NftProperty extends Model
{
    use HasFactory;

    protected $hidden = ['id', 'unique_id'];

    protected $appends = ['nft_property_id', 'nft_property_unique_id'];
    
    public function getNftPropertyIdAttribute() {

        return $this->id;
    }

    public function getNftPropertyUniqueIdAttribute() {

        return $this->unique_id;
    }

    public function nftImages() {

        return $this->hasMany(NftImage::class,'nft_property_id');
    }

    public static function boot() {

        parent::boot();

        static::creating(function ($model) {

            $model->attributes['unique_id'] = "NP"."-".uniqid();

        });

        static::created(function($model) {

            $model->attributes['unique_id'] = "NP"."-".$model->attributes['id']."-".uniqid();

            $model->save();
        
        });

        static::deleting(function ($model){

            $model->nftImages()->delete();

        });

    }
}
