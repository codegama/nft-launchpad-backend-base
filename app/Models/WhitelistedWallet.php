<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WhitelistedWallet extends Model
{
    use HasFactory;

    protected $hidden = ['id', 'unique_id'];

    protected $appends = ['whitelisted_wallet_id', 'whitelisted_wallet_unique_id'];

    protected $fillable = [ 'user_id', 'contract_id', 'wallet_address'];
    
    public function getWhitelistedWalletIdAttribute() {

        return $this->id;
    }

    public function getWhitelistedWalletUniqueIdAttribute() {

        return $this->unique_id;
    }

    public static function boot() {

        parent::boot();

        static::creating(function ($model) {

            $model->attributes['unique_id'] = "WWA"."-".uniqid();

        });

        static::created(function($model) {

            $model->attributes['unique_id'] = "WWA"."-".$model->attributes['id']."-".uniqid();

            $model->save();
        
        });

    }
}
