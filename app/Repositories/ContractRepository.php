<?php

namespace App\Repositories;

use App\Helpers\Helper;

use Log, Validator, Setting, Exception, DB;

use App\Models\User, App\Models\Contract, App\Models\NftImage;

class ContractRepository {

	/**
     * @method contracts_list_response()
     *
     * @uses Format the follow user response
     *
     * @created Arun
     * 
     * @updated 
     *
     * @param object $request
     *
     * @return object $payment
     */

    public static function contracts_list_response($contracts, $request) {

        $contracts = $contracts->map(function ($contract, $key) use ($request) {

                        $is_presale_started = $is_publicsale_started = NO;

                        if ($request->id) {

                            $user = User::find($request->id);

                            $contract->pre_sale_start_date = common_date($contract->pre_sale_start_date, ($user->timezone ?? "America/New_York"), "Y-m-d H:i:s");
                            
                            $contract->public_sale_start_date = common_date($contract->public_sale_start_date, ($user->timezone ?? "America/New_York"), "Y-m-d H:i:s");

                            $request->request->add(['timezone' => ($user->timezone ?? "America/New_York")]);
                        }

                        $current_date = strtotime(common_date(date("Y-m-d H:i:s"), $request->timezone, "Y-m-d H:i:s"));

                        $contract->pre_sale_start_date_formatted = common_date($contract->pre_sale_start_date, "", 'd M Y H:i:s');

                        $contract->public_sale_start_date_formatted = common_date($contract->public_sale_start_date, "", 'd M Y H:i:s');

                        $pre_sale_start_date = strtotime($contract->pre_sale_start_date);

                        $public_sale_start_date = strtotime($contract->public_sale_start_date);

                        if ($current_date > $pre_sale_start_date) {
                            
                            $is_presale_started = $current_date < $public_sale_start_date ? YES : NO;

                            $is_publicsale_started = $current_date > $public_sale_start_date ? YES : NO;
                        }

                        $contract->is_presale_started = $is_presale_started;

                        $contract->is_publicsale_started = $is_publicsale_started;
                        
                        return $contract;
                    });

    	return $contracts ?: emptyObject();

    }

    /**
     * @method contracts_single_response()
     *
     * @uses Format the follow user response
     *
     * @created Arun
     * 
     * @updated 
     *
     * @param object $request
     *
     * @return object $payment
     */

    public static function contracts_single_response($contract, $request) {

        $is_presale_started = $is_publicsale_started = NO;

        $timezone = $request->timezone ?? "America/New_York";

        if ($request->id) {

            $user = User::find($request->id);

            $timezone = $user->timezone ?? "America/New_York";

        }

        $current_date = strtotime(common_date(date("Y-m-d H:i:s"), $timezone, "Y-m-d H:i:s"));

        $pre_sale_start_date = common_date($contract->pre_sale_start_date, $timezone, "Y-m-d H:i:s");
        
        $public_sale_start_date = common_date($contract->public_sale_start_date, $timezone, "Y-m-d H:i:s");

        $contract->pre_sale_start_date = $pre_sale_start_date;

        $contract->public_sale_start_date = $public_sale_start_date;

        $contract->pre_sale_start_date_formatted = common_date($pre_sale_start_date, "", 'd M Y H:i:s');

        $contract->public_sale_start_date_formatted = common_date($public_sale_start_date, "", 'd M Y H:i:s');

        $pre_sale_start_date = strtotime($pre_sale_start_date);

        $public_sale_start_date = strtotime($public_sale_start_date);

        if ($current_date > $pre_sale_start_date) {
            
            $is_presale_started = $current_date < $public_sale_start_date ? YES : NO;

            $is_publicsale_started = $current_date > $public_sale_start_date ? YES : NO;
        }

        $contract->is_presale_started = $is_presale_started;

        $contract->is_publicsale_started = $is_publicsale_started;

        return $contract ?: emptyObject();

    }

}
