<?php

namespace App\Repositories;

use App\Helpers\Helper;

use Log, Validator, Setting, Exception, DB;

use App\Models\User, App\Models\ProjectOwnerTransaction;

class ProjectRepository {

	/**
     * @method projects_list_response()
     *
     * @uses Format the follow user response
     *
     * @created vithya R
     * 
     * @updated vithya R
     *
     * @param object $request
     *
     * @return object $payment
     */

    public static function projects_list_response($projects, $request) {

        $projects = $projects->map(function ($project, $key) use ($request) {

                        $project->start_time_formatted = common_date($project->start_time, $request->timezone, 'd M Y H:i:s');

                        $project->end_time_formatted = common_date($project->start_time, $request->timezone, 'd M Y H:i:s');

                        $project_transaction = ProjectOwnerTransaction::where('user_id',  $request->id)->where('project_id',  $project->id)->first();

                        $project->is_paid = $project_transaction ? YES : NO;

                        return $project;
                    });

    	return $projects ?: emptyObject();

    }
    
    /**
     * @method projects_single_response()
     *
     * @uses Format the follow user response
     *
     * @created vithya R
     * 
     * @updated vithya R
     *
     * @param object $request
     *
     * @return object $payment
     */

    public static function projects_single_response($project, $request) {

        $project->start_time_formatted = common_date($project->start_time, $request->timezone, 'd M Y h:i A');

        $project->end_time_formatted = common_date($project->end_time, $request->timezone, 'd M Y h:i A');

        if ($request->id) {
            
            $user = User::find($request->id);

            $project->start_time_formatted = common_date($project->start_time, ($user->timezone ?? "America/New_York"), 'd M Y h:i A');

            $project->end_time_formatted = common_date($project->end_time, ($user->timezone ?? "America/New_York"), 'd M Y h:i A');

            $project->end_time = common_date($project->end_time, ($user->timezone ?? "America/New_York"), "Y-m-d H:i:s");

            $next_round_start_time = date('Y-m-d H:i:s',strtotime('+'.$project->next_round_start_time.' minutes',strtotime($project->start_time)));

            $project->round_2_start_time = common_date($next_round_start_time, ($user->timezone ?? "America/New_York"), "Y-m-d H:i:s");

        }

        return $project ?: emptyObject();

    }

}
