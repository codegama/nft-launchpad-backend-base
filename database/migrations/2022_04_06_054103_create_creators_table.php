<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCreatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('role')->after('email')->default(USER);
        });

        Schema::create('creator_applications', function (Blueprint $table) {
            $table->id();
            $table->string('unique_id')->default(rand());
            $table->integer('user_id')->default(0);
            $table->string('creator_name');
            $table->tinyInteger('agree_terms')->default(YES);
            $table->integer('mint_time_period')->default(1);//In days
            $table->integer('twitter_followers')->default(0);
            $table->string('project_artwork')->default('');
            $table->tinyInteger('is_collection_derivative')->default(YES);
            $table->string('discord_id')->default('');
            $table->string('email')->default('');
            $table->text('project_description')->nullable();
            $table->text('project_long_term_goal')->nullable();
            $table->text('about_team')->nullable();
            $table->text('team_link_attachment')->nullable();
            $table->text('partnerships')->nullable();
            $table->text('partnerships_link_attachment')->nullable();
            $table->text('investment')->nullable();
            $table->text('investment_link_attachment')->nullable();
            $table->text('whitepaper_roadmap')->nullable();
            $table->text('whitepaper_roadmap_link_attachment')->nullable();
            $table->text('artwork')->nullable();
            $table->string('artwork_picture')->default(asset('artwork.png'));
            $table->string('website_link')->default("");
            $table->string('instagram_link')->default("");
            $table->string('snapchat_link')->default("");
            $table->string('facebook_link')->default("");
            $table->string('twitter_link')->default("");
            $table->string('linkedin_link')->default("");
            $table->string('pinterest_link')->default("");
            $table->string('youtube_link')->default("");
            $table->string('twitch_link')->default("");
            $table->string('discord_link')->default("");
            $table->string('medium_link')->default("");
            $table->string('telegram_link')->default("");
            $table->string('external_link')->default("");
            $table->datetime('mint_date')->nullable();
            $table->integer('items_count')->default(0);
            $table->tinyInteger('is_team_dox')->default(YES);
            $table->string('mint_amount')->default("");
            $table->text('questions')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('contracts', function (Blueprint $table) {
            $table->id();
            $table->string('unique_id')->default(rand());
            $table->integer('user_id')->default(0);
            $table->string('contract_name')->default("");
            $table->string('symbol')->default("");
            $table->string('total_supply')->default(0);
            $table->text('description')->nullable();
            $table->string('wallet_address')->default("");
            $table->string('transaction_hash')->default("");
            $table->string('contract_address')->default("");
            $table->tinyInteger('is_deployed')->default(NO);
            $table->tinyInteger('deploye_access')->default(DEPLOY_ACCESS_NONE);
            $table->datetime('pre_sale_start_date')->nullable();
            $table->datetime('public_sale_start_date')->nullable();
            $table->string('pre_sale_mint_fee')->default("");
            $table->string('public_sale_mint_fee')->default("");
            $table->integer('total_nfts')->default(0);
            $table->integer('total_pre_sale_nfts')->default(0);
            $table->integer('total_public_sale_nfts')->default(0);
            $table->tinyInteger('restrict_whitelisted_wallet')->default(YES);
            $table->string('sample_nft')->default(asset('artwork.png'));
            $table->tinyInteger('status')->default(APPROVED);
            $table->timestamps();
        });

        Schema::create('nft_properties', function (Blueprint $table) {
            $table->id();
            $table->string('unique_id')->default(rand());
            $table->integer('contract_id')->default(0);
            $table->string('name')->default("");
            $table->tinyInteger('status')->default(APPROVED);
            $table->timestamps();
        });

        Schema::create('merge_images', function (Blueprint $table) {
            $table->id();
            $table->string('unique_id')->default(rand());
            $table->integer('contract_id')->default(0);
            $table->string('file_name')->nullable();
            $table->string('picture')->nullable();
            $table->tinyInteger('mint_status')->default(MINT_PENDING);
            $table->tinyInteger('status')->default(APPROVED);
            $table->timestamps();
        });

        Schema::create('merge_image_properties', function (Blueprint $table) {
            $table->id();
            $table->string('unique_id')->default(rand());
            $table->integer('merge_image_id')->default(0);
            $table->integer('nft_property_id')->default(0);
            $table->string('value')->nullable();
            $table->tinyInteger('status')->default(APPROVED);
            $table->timestamps();
        });

        Schema::create('merge_image_payments', function (Blueprint $table) {
            $table->id();
            $table->string('unique_id')->default(uniqid());
            $table->integer('merge_image_id')->default(0);
            $table->integer('user_id')->default(0);
            $table->integer('to_user_id')->default(0);
            $table->string('minted_wallet_address')->nullable();
            $table->tinyInteger('status')->default(APPROVED);
            $table->timestamps();
        });

        Schema::create('nft_images', function (Blueprint $table) {
            $table->id();
            $table->string('unique_id')->default(rand());
            $table->integer('contract_id')->default(0);
            $table->integer('nft_property_id')->default(0);
            $table->string('picture')->nullable();
            $table->string('name')->default("");
            $table->string('value')->default("");
            $table->tinyInteger('is_used')->default(YES);
            $table->tinyInteger('status')->default(APPROVED);
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('role');
        });
        
        Schema::dropIfExists('creator_applications');
        Schema::dropIfExists('contracts');
        Schema::dropIfExists('nft_properties');
        Schema::dropIfExists('merge_images');
        Schema::dropIfExists('merge_image_properties');
        Schema::dropIfExists('merge_image_payments');
        Schema::dropIfExists('nft_images');
    }
}
