<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWhitelistedWalletsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('whitelisted_wallets', function (Blueprint $table) {
            $table->id();
            $table->string('unique_id')->default(rand());
            $table->integer('user_id')->default(0);
            $table->integer('contract_id')->default(0);
            $table->string('wallet_address')->default("");
            $table->timestamps();
        });

        Schema::table('merge_images', function (Blueprint $table) {
            $table->string('minted_wallet_address')->default("")->after('picture');
        });

        Schema::table('contracts', function (Blueprint $table) {
            $table->tinyInteger('publish_status')->default(CONTACT_UPCOMING)->after('sample_nft');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('whitelisted_wallets');

        Schema::table('merge_images', function (Blueprint $table) {
            $table->dropColumn('minted_wallet_address');
        });

        Schema::table('contracts', function (Blueprint $table) {
            $table->dropColumn('publish_status');
        });
    }
}
