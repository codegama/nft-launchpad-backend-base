<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMintFeeRelatedFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('contracts', function (Blueprint $table) {
            $table->tinyInteger('is_presale_fee_set')->default(NO)->after('pre_sale_mint_fee');
            $table->tinyInteger('is_publicsale_fee_set')->default(NO)->after('public_sale_mint_fee');
            $table->tinyInteger('is_release_limit_set')->default(NO)->after('total_public_sale_nfts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::table('contracts', function (Blueprint $table) {
            $table->dropColumn('is_presale_fee_set');
            $table->dropColumn('is_publicsale_fee_set');
        });
    }
}
