<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class StakeTokenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('settings')->insert([
            [
                'key' => 'max_stake_token',
                'value' => '10000'
            ],
        ]);
    }
}
