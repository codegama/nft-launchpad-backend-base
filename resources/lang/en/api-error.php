<?php

return [

    // Authentication
    1000 => 'Your account is waiting for admin approval',

    1001 => 'Please verify your email address!!',

    1002 => 'You are not a registered user!!',

    1003 => 'Your session Expired',

    1004 => 'Invalid session. Login again',

    1005 => 'Authentication parameters are invalid.!',

    1006 => 'The record not exists.',

    1007 => 'Your account is waiting for admin approval.',

    1008 => 'Mobile verification pending.',

    1009 => 'Invalid OTP ! Please check',

    1010 => 'You are not a invited user!!',

    1011 => 'You are not a registered creator!!',

    1012 => 'Unauthorized token',

    1013 => 'Failed to logout, please try again.',
    

    101 => 'Invalid Input',

    102 => 'Sorry, the username or password you entered do not match.',

    103 => 'Oops! something went wrong. We couldn’t save your changes.',

    104 => 'Invalid email address',

    105 => 'The mail send process is failed!!!',

    106 => 'The mail configuration failed!!!',

    107 => 'The payment configuration is failed',

    108 => 'Sorry, the password is not matched.',

    117 => 'The requested email is disabled by admin.',

    118 => 'The change password only available for manual login.',

    119 => 'Account delete failed',

    120 => 'The card record is not found.',

    125 => 'The mobile number Invalid. Not a registered Mobile Number',

    126 => 'The SMS send is failed due to some technical errors. Please try again.',

    128 => 'Details update failed!!',
    
    130 =>  'The seleced user is not found',

    131 => 'Your wallet balance is low.',

    132 => 'The user is invalid',

    133 => 'The default card is not found. Please add card and try again',

    135 => "The user is not found",

    136 => 'please add card and try again!!',

    140 => 'The withdraw request is not found',

    141 => 'The withdraw cancel is not allowed',

    142 => 'Your wallet balance is low.',

    143 => 'The requested amount is less than the min balance(:other_key)',

    145 => 'Already paid',

    151 => 'The subscription record not found.',

    152 => 'The payment record not found.',

    156 => 'Your account is not yet verified.',

    157 => 'Please verify your email and continue',

    158 => 'Your documents are waiting for admin approval',

    159 => 'Your document verification declined. Please check and reupload',

    160 => 'Upload the documents for verification',

    163 =>  'Invalid Token', 

    167 =>  'Incorrect password',

    168 =>  'BUSDX token is less than minimum',
    
    170 => 'Requested amount should be less than wallet balance',

    181 => 'That username already taken. Please try another',

    182 => 'No subscription plans available',

    183 => 'Remaining token is less than stack tokens',

    184 => 'Round 2 started, please reload and stake for round 2',

    185 => 'Round 2 is completed',

    187 => 'Please check whether image files in zip are equal to import excel data',

    186 => 'Sorry, the email or password you entered do not match.',

    188 => 'Properties name should be unique',

    189 => 'Wallet Address already a creator',

    190 => 'Property details not found',

    191 => 'Nft Properties delete failed',

    192 => 'Restrict Whitelisted Wallet status update failed',

    193 => 'All Nfts are minted',

    194 => 'The selected Nft is not available to mint',

    195 => 'All Pre-sale Nfts are minted',

    196 => 'Your wallet is not Whitelisted',

    300 => 'The project update failed',

    301 => 'The token payment update failed',

    302 => 'The selected project is not available',

    303 => 'You can\'t invest on your project',

    304 => 'The project purchase limit is exceeds',

    305 => 'The transaction update failed',

    306 => 'Allowed tokens exceeds.',

    307 => 'Invalid Token',

    308 => 'Project limit exceeds. Please subscribe and continue',

    309 => 'Invalid verification code',

    310 => 'action failed',

    311 => 'User staked amount save failed',

    312 => 'Merge Image Details Not Found',

    313 => 'Contract Details Not Found',
];
