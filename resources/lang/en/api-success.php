<?php

return [

    101 => 'Successfully loggedin!!',

    102 => 'Mail sent successfully',

    103 => 'Your account deleted.',

    104 => 'Your password changed.',

    105 => 'The card added to your account.',

    106 => 'Logged out',

    108 => 'The card changed to default.',

    109 => 'The card deleted',

    110 => 'The card marked as default.',

    111 => 'The profile updated',

    112 => 'The billing account added',

    113 => 'The billing account deleted',

    114 => 'The document uploaded and waiting for approval',

    115 => 'The uploaded document deleted',

    116 => 'The uploaded documents are deleted',

    117 => 'Thanks, Your email address is verified.',

    118 => 'Your payment done successfully.',

    119 => 'You are eligible for subscription',

    120 => 'Project Payment Saved successfully',

    121 => 'Creators Application submitted successfully!!',

    122 => 'Contact created successfully!!',

    123 => 'Contact updated successfully!!',

    124 => 'Contact deleted successfully!!',

    125 => 'Contact deploy access updated successfully!!',

    126 => 'Contact deployed successfully!!',

    127 => 'Token refresh success',

    128 => 'Token is active',

    129 => 'Nft Properties deleted successfully',

    130 => 'Nft image mint status updated successfully',

    131 => 'Nft merge images uploaded successfully',

    132 => 'Nft merge images updated successfully',

    133 => 'Whitelisted Wallet imported successfully',

    134 => 'Restrict Whitelisted Wallet status updated successfully',

    135 => 'Nft Properties saved successfully',

    136 => 'Mint Fee updated successfully!!',

    137 => 'Release Limit updated successfully!!',

    300 => 'The project saved.',

    301 => 'The project updated',

    302 => 'The project deleted',

    303 => 'Token payment details updated',

    304 => 'The transaction updated',

    305 => 'Valid Token',

    306 => 'Investment claimed successfully',

    307 => 'Payment status updated',

    308 => 'The verification code sent to your email address',

    309 => 'Saved',

    310 => 'The pool contract updated',

    311 => 'The Contact form submitted successfully',

    312 => 'User Staked amount saved successfully',

    313 => 'User Unstaked amount saved successfully',

    314 => 'Merge Image Deleted Successfully',

];