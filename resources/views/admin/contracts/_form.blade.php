<div class="row">

    <div class="col-lg-12 col-12">

        <div class="box">
            <div class="box-header with-border">
                
                <h3 class="box-title">{{ $contract->id ? tr('edit_contract') : tr('add_contract') }}</h3>

            </div>
                
            <form action="{{ (Setting::get('is_demo_control_enabled') == YES) ? '#' : route('admin.contracts.save') }}" method="POST" enctype="multipart/form-data" role="form">
                
                @csrf

                <div class="box-body">

                    <div class="row">

                        <input type="hidden" name="contract_id" id="contract_id" value="{{ $contract->id}}">

                        <div class="col-md-6">
                            <label>{{ tr('creators') }} *</label>
                            <div class="form-group">
                                <select class="form-control select2 w-p100" name="creator_id" required>

                                    <option value="">Select Creators</option>

                                    @foreach($creators as $creator)
                                        <option value="{{$creator->id ?? "0"}}"
                                            @if($creator->is_selected == YES || $creator->id == $contract->user_id)       
                                                selected="selected" 
                                            @endif
                                        >
                                            {{$creator->name ?: ""}}
                                        </option>
                                    @endforeach

                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                                
                            <label for="name">{{ tr('contract_name') }}*</label>

                            <div class="form-group">                                  
                                
                                <input type="text" id="contract_name" name="contract_name" class="form-control" placeholder="{{ tr('contract_name') }}" value="{{old('contract_name') ?: $contract->contract_name}}" required>

                            </div>

                        </div>

                        <div class="col-md-6">
                                
                            <label for="symbol">{{ tr('symbol') }}*</label>

                            <div class="form-group">                                  
                                
                                <input type="text" id="symbol" name="symbol" class="form-control" placeholder="{{ tr('symbol') }}" value="{{old('symbol') ?: $contract->symbol}}" required>

                            </div>

                        </div>

                        <div class="col-md-6">
                                
                            <label for="total_supply">{{ tr('total_supply') }}*</label>

                            <div class="form-group">                                  
                                
                                <input type="number" min="1" step="any" id="total_supply" name="total_supply" class="form-control" placeholder="{{ tr('total_supply') }}" value="{{old('total_supply') ?: $contract->total_supply}}" required>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <label for="pre_sale_start_date">{{tr('pre_sale_start_date')}} *</label>

                            <div class="form-group">
                                    
                                <input class="form-control datetimepicker" name="pre_sale_start_date" type="text" value="{{ old('pre_sale_start_date') ?: common_date($contract->pre_sale_start_date, Auth::guard('admin')->user()->timezone, "Y-m-d H:i:s")}}">

                            </div>
                        </div>

                        <div class="col-md-6">

                            <label for="public_sale_start_date">{{tr('public_sale_start_date')}} *</label>

                            <div class="form-group">
                                    
                                <input class="form-control datetimepicker" name="public_sale_start_date" type="text" value="{{ old('public_sale_start_date') ?: common_date($contract->public_sale_start_date, Auth::guard('admin')->user()->timezone, "Y-m-d H:i:s")}}">

                            </div>
                        </div>

                        <div class="col-md-6">
                                
                            <label for="pre_sale_mint_fee">{{ tr('pre_sale_mint_fee') }}*</label>

                            <div class="form-group">                                  
                                
                                <input type="number" min="0" step="any" id="pre_sale_mint_fee" name="pre_sale_mint_fee" class="form-control" placeholder="{{ tr('pre_sale_mint_fee') }}" value="{{old('pre_sale_mint_fee') ?: $contract->pre_sale_mint_fee}}" required>

                            </div>

                        </div>

                        <div class="col-md-6">
                                
                            <label for="public_sale_mint_fee">{{ tr('public_sale_mint_fee') }}*</label>

                            <div class="form-group">                                  
                                
                                <input type="number" min="0" step="any" id="public_sale_mint_fee" name="public_sale_mint_fee" class="form-control" placeholder="{{ tr('public_sale_mint_fee') }}" value="{{old('public_sale_mint_fee') ?: $contract->public_sale_mint_fee}}" required>

                            </div>

                        </div>

                        <!-- <div class="col-md-6">
                                
                            <label for="total_nfts">{{ tr('total_nfts') }}*</label>

                            <div class="form-group">                                  
                                
                                <input type="number" min="1" step="any" id="total_nfts" name="total_nfts" class="form-control" placeholder="{{ tr('total_nfts') }}" value="{{old('total_nfts') ?: $contract->total_nfts}}" required>

                            </div>

                        </div>
 -->
                        <div class="col-md-6">
                                
                            <label for="total_pre_sale_nfts">{{ tr('total_pre_sale_nfts') }}*</label>

                            <div class="form-group">                                  
                                
                                <input type="number" min="1" step="any" id="total_pre_sale_nfts" name="total_pre_sale_nfts" class="form-control" placeholder="{{ tr('total_pre_sale_nfts') }}" value="{{old('total_pre_sale_nfts') ?: $contract->total_pre_sale_nfts}}" required>

                            </div>

                        </div>

                        <div class="col-md-6">
                                
                            <label for="total_public_sale_nfts">{{ tr('total_public_sale_nfts') }}*</label>

                            <div class="form-group">                                  
                                
                                <input type="number" min="1" step="any" id="total_public_sale_nfts" name="total_public_sale_nfts" class="form-control" placeholder="{{ tr('total_public_sale_nfts') }}" value="{{old('total_public_sale_nfts') ?: $contract->total_public_sale_nfts}}" required>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <label>{{ tr('select_sample_nft') }}</label>
                            <p class="text-muted mt-0 mb-0">{{tr('image_validate')}}</p>
                            <div class="form-group">

                                <input type="file" class="form-control"  id="sample_nft" name="sample_nft"  accept="image/png, image/jpg"  >
                            
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="checkbox" name="restrict_whitelisted_wallet" id="md_checkbox_27" class="filled-in chk-col-light-blue" {{$contract->restrict_whitelisted_wallet == YES ? 'checked' : "" }} />
                                <label for="md_checkbox_27">{{ tr('restrict_whitelisted_wallet') }}*</label>
                            </div>
                        </div>

                        <div class="col-md-12">

                            <label>{{ tr('description') }} *</label>

                            <div class="form-group">

                                <textarea name="description" rows="5" class="form-control" required>{{ $contract->description ?: old('description') }}</textarea>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="box-footer">
                    
                    <button type="reset" class="btn btn-warning btn-default btn-squared px-30">Reset</button>

                    <button type="submit" class="btn btn-info pull-right">Submit</button>

                </div>

            </form>

        </div>

    </div>

</div>