@extends('layouts.admin')

@section('title', tr('contracts'))

@section('content-header', tr('contracts'))

@section('breadcrumb')

	<li class="breadcrumb-item"><a href="{{route('admin.contracts.index')}}">{{tr('contracts')}}</a></li>

    <li class="breadcrumb-item active">{{tr('add_contract')}}</a></li>

@endsection

@section('content')

	@include('admin.contracts._form')

@endsection