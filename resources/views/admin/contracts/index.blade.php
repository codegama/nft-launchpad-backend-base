@extends('layouts.admin')

@section('title', tr('contracts'))

@section('content-header', tr('contracts'))

@section('breadcrumb')

    <li class="breadcrumb-item active">
        <a href="{{route('admin.contracts.index')}}">{{ tr('contracts') }}</a>
    </li>

    <li class="breadcrumb-item">{{tr('list_contracts')}}</li>

@endsection

@section('content')

<div class="row">
    <div class="col-md-12">

        <div class="box">

            <div class="box-header with-border">
                
                <h3 class="box-title">{{tr('list_contracts')}}</h3>

            </div>

            <div class="box-body">

                <!-- <div class="callout bg-pale-secondary">
                    <h4>Notes:</h4>
                    <p>
                        <ul>
                            <li>All the projects added by the project owner will be displayed here with basic information. </li>
                        </ul>
                    </p>
                </div> -->

                <form method="GET" action="{{route('admin.contracts.index')}}">

                    <div class="row">

                        <input type="hidden" id="contract_id" name="contract_id" value="{{Request::get('contract_id') ?? ''}}">

                        <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 resp-mrg-btm-md">
                            @if(Request::has('search_key'))
                            <p class="text-muted">Search results for: <b>{{Request::get('search_key')}}</b></p>
                            @endif
                        </div>

                        <div class="col-xs-12 col-sm-12 col-lg-3 col-md-3 offset-lg-3 offset-md-3 md-full-width resp-mrg-btm-md">

                            <select class="form-control select2" name="status">

                                <option class="select-color" value="">{{tr('select_status')}}</option>

                                <option class="select-color" value="{{CONTACT_UPCOMING}}" @if(Request::get('status') == CONTACT_UPCOMING && Request::get('status')!='' ) selected @endif>{{tr('upcoming')}}</option>

                                <option class="select-color" value="{{CONTACT_OPENED}}" @if(Request::get('status') == CONTACT_OPENED && Request::get('status')!='' ) selected @endif>{{tr('opened')}}</option>

                                <option class="select-color" value="{{CONTACT_COMPLETED}}" @if(Request::get('status') == CONTACT_COMPLETED && Request::get('status')!='' ) selected @endif>{{tr('completed')}}</option>

                            </select>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-lg-6 col-md-12">

                            <div class="input-group">

                                <input type="text" class="form-control" name="search_key" value="{{Request::get('search_key')??''}}" placeholder="{{tr('contracts_search_placeholder')}}"> 

                                <span class="input-group-btn">
                                    &nbsp

                                    <button type="submit" class="btn btn-default reset-btn">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </button>
                                    @if(Request::get('contract_id'))
                                    <a href="{{route('admin.contracts.index',['contract_id'=>Request::get('contract_id') ?? '', ])}}" class="btn btn-default reset-btn">
                                        <span> <i class="fa fa-eraser" aria-hidden="true"></i>
                                        </span>
                                    </a>
                                    @else
                                    <a href="{{route('admin.contracts.index')}}" class="btn btn-default reset-btn">
                                        <span> <i class="fa fa-eraser" aria-hidden="true"></i>
                                        </span>
                                    </a>
                                    @endif
                                </span>

                            </div>

                        </div>

                    </div>

                </form>
                <br>

                <div class="table-responsive">
                    
                    <table id="example2" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                        
                        <thead>
                            
                            <tr>
                                <th>{{ tr('s_no') }}</th>
                                <th>{{ tr('contract_name') }}</th>
                                <th>{{ tr('token_symbol') }}</th>
                                <th>{{ tr('creator_name') }}</th>
                                <th>{{ tr('presale_postsale_time') }}</th>
                                <th>{{ tr('status') }}</th>
                                <th>{{ tr('action') }}</th>
                            </tr>

                        </thead>
                        <tbody>
                            
                            @foreach($contracts as $i => $contract)

                                <tr>

                                    <td>{{ $i+$contracts->firstItem() }}</td>

                                    <td>
                                        <small><a class="text-yellow hover-warning" href="{{route('admin.contracts.view' , ['contract_id' => $contract->id])}}">{{$contract->contract_name ?? tr('not_available')}}</a></small>
                                    </td>

                                    <td>

                                        <h6 class="text-muted">{{$contract->symbol ?: tr('not_available')}}</h6>

                                    </td>

                                    <td class="white-space-nowrap">
                                        <a href="{{route('admin.creators.view' , ['creator_id' => $contract->user_id])}}" class="custom-a">
                                            {{$contract->creator->name ?? tr('not_available')}}
                                        </a>
                                        
                                    </td>

                                    <td>{{ common_date($contract->pre_sale_start_date, Auth::guard('admin')->user()->timezone) }} - <br>{{ common_date($contract->public_sale_start_date, Auth::guard('admin')->user()->timezone) }}</td>


                                    <td>
                                        @if($contract->status == APPROVED)

                                            <span class="label label-success">{{ tr('approved') }}</span>

                                        @else

                                            <span class="label label-warning">{{ tr('declined') }}</span>

                                        @endif
                                    </td>
                                   
                                    <td>

                                        <div class="btn-group" role="group">

                                            <button type="button" class="btn btn-primary dropdown-toggle dropdown-menu-right" id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ft-settings icon-left"></i> {{ tr('action') }}</button>

                                            <div class="dropdown-menu action-dropdown-menu" aria-labelledby="btnGroupDrop1">

                                                <a class="dropdown-item" href="{{route('admin.contracts.view' , ['contract_id' => $contract->id])}}">&nbsp;{{ tr('view') }}</a>


                                                @if(Setting::get('is_demo_control_enabled') == YES)

                                                <a class="dropdown-item" href="javascript:void(0)">&nbsp;{{ tr('edit') }}</a>

                                                <a class="dropdown-item" href="javascript:void(0)">&nbsp;{{ tr('delete') }}</a>

                                                @else

                                                <a class="dropdown-item" href="{{ route('admin.contracts.edit', ['contract_id' => $contract->id] ) }}">&nbsp;{{ tr('edit') }}</a>
                                               
                                                <a class="dropdown-item" onclick="return confirm(&quot;{{ tr('contract_delete_confirmation' , $contract->contract_name) }}&quot;);" href="{{ route('admin.contracts.delete', ['contract_id' => $contract->id,'page'=>request()->input('page')] ) }}">&nbsp;{{ tr('delete') }}</a>

                                                @endif

                                                @if($contract->status == APPROVED)

                                                <a class="dropdown-item" href="{{  route('admin.contracts.status' , ['contract_id' => $contract->id] )  }}" onclick="return confirm(&quot;{{ $contract->contract_name }} - {{ tr('contract_decline_confirmation') }}&quot;);">&nbsp;{{ tr('decline') }}
                                                </a>

                                                @else

                                                <a class="dropdown-item" href="{{ route('admin.contracts.status' , ['contract_id' => $contract->id] ) }}">&nbsp;{{ tr('approve') }}</a>

                                                @endif

                                                @if(($contract->deploye_access == DEPLOY_ACCESS_REQUESTED && $contract->deploye_access != DEPLOY_ACCESS_GRANTED) || $contract->deploye_access == DEPLOY_ACCESS_DECLINED)

                                                    <a class="dropdown-item" href="{{ route('admin.contracts.deploy_access', ['contract_id' => $contract->id, 'deploye_access' => DEPLOY_ACCESS_GRANTED] ) }}">&nbsp;{{ tr('grant_deploy_access') }}</a>

                                                @endif

                                                @if($contract->deploye_access == DEPLOY_ACCESS_REQUESTED || $contract->deploye_access == DEPLOY_ACCESS_GRANTED)

                                                    <a class="dropdown-item" href="{{ route('admin.contracts.deploy_access', ['contract_id' => $contract->id, 'deploye_access' => DEPLOY_ACCESS_DECLINED] ) }}" onclick="return confirm(&quot;{{ tr('decline_deploy_access_confirmation' , $contract->contract_name) }}&quot;);">&nbsp;{{ tr('decline_deploy_access') }}
                                                    </a>

                                                @endif

                                                <a class="dropdown-item" href="{{route('admin.nfts.index' , ['contract_id' => $contract->id])}}">&nbsp;{{ tr('nfts') }}</a>

                                            </div>

                                        </div>

                                    </td>

                                </tr>
                                
                                @endforeach

                        </tbody>
                    </table>
                </div>
            </div>

            <div class="box-footer clearfix">
                    
                <div class="pull-right rd-flex">{{ $contracts->appends(request()->input())->links('pagination::bootstrap-4') }}</div>
            </div>

        </div>

    </div>
</div>


@endsection