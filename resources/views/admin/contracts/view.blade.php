@extends('layouts.admin')

@section('title', tr('contracts'))

@section('content-header', tr('contract'))

@section('breadcrumb')

    <li class="breadcrumb-item active">
        <a href="{{route('admin.contracts.index')}}">{{ tr('contracts') }}</a>
    </li>

    <li class="breadcrumb-item">{{tr('view_contract')}}</li>

@endsection

@section('content')

<div class="box">
    
    <div class="box-body">

        <div class="row">
            
            <div class="col-md-12 d-flex">
                <div class="col-md-5">
                    <div class="media-list media-list-divided">

                        <div class="media media-single">

                            <img class="w-80 border-2" src="{{$contract->sample_nft}}" alt="...">
                            
                            <div class="media-body">
                                <h6>{{$contract->contract_name}}</h6>
                                <small class="text-fader">{{common_date($contract->created_at , Auth::guard('admin')->user()->timezone)}}</small>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="media-list media-list-divided">

                        @if($contract->status == APPROVED)

                            <a class="btn btn-warning margin" href="{{  route('admin.contracts.status', ['contract_id' => $contract->id])}}" onclick="return confirm(&quot;{{ $contract->contract_name }} - {{ tr('contract_decline_confirmation') }}&quot;);">
                                    <i class="fa fa-check"></i> {{ tr('decline') }}
                            </a>

                        @else

                            <a class="btn bg-navy margin" href="{{ route('admin.contracts.status' , ['contract_id' => $contract->id] ) }}"> <i class="fa fa-check"></i>  {{ tr('approve') }}</a>

                        @endif

                        @if(Setting::get('is_demo_control_enabled') == YES)

                            <button class="btn bg-purple margin"><i class="fa fa-edit"></i> {{tr('edit')}}</button>

                            <button class="btn bg-olive margin"><i class="fa fa-trash"></i> {{tr('delete')}}</button>

                        @else

                            <a class="btn bg-purple margin" href="{{ route('admin.contracts.edit', ['contract_id' => $contract->id] ) }}"><i class="fa fa-edit"></i> {{tr('edit')}}</a>

                            <a class="btn bg-warning margin" onclick="return confirm(&quot;{{ tr('contract_delete_confirmation' , $contract->contract_name) }}&quot;);" href="{{ route('admin.contracts.delete', ['contract_id' => $contract->id] ) }}"><i class="fa fa-trash"></i> {{tr('delete')}}</a>

                        @endif

                        @if(($contract->deploye_access == DEPLOY_ACCESS_REQUESTED && $contract->deploye_access != DEPLOY_ACCESS_GRANTED) || $contract->deploye_access == DEPLOY_ACCESS_DECLINED)

                            <a class="btn bg-olive margin" href="{{ route('admin.contracts.deploy_access', ['contract_id' => $contract->id, 'deploye_access' => DEPLOY_ACCESS_GRANTED] ) }}"><i class="fa fa-edit"></i> {{tr('grant_deploy_access')}}</a>

                        @endif

                        @if($contract->deploye_access == DEPLOY_ACCESS_REQUESTED || $contract->deploye_access == DEPLOY_ACCESS_GRANTED)

                            <a class="btn btn-warning margin" onclick="return confirm(&quot;{{ tr('decline_deploy_access_confirmation' , $contract->contract_name) }}&quot;);" href="{{ route('admin.contracts.deploy_access', ['contract_id' => $contract->id, 'deploye_access' => DEPLOY_ACCESS_DECLINED] ) }}"><i class="fa fa-trash"></i> {{tr('decline_deploy_access')}}</a>

                        @endif

                        <a class="btn bg-navy margin" href="{{route('admin.nfts.index' , ['contract_id' => $contract->id])}}"> <i class="fa fa-check"></i>  {{ tr('nfts') }}</a>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    
    <div class="col-lg-6 col-12">
        
        <div class="box">
            
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover no-margin">
                        <tbody>
                            <tr>
                                <td>{{tr('unique_id')}}</td>
                                <td>{{$contract->contract_unique_id ?: tr('not_available')}}</td>
                            </tr>
                            <tr>
                                <td>{{tr('creator')}}</td>
                                <td><a href="{{route('admin.creators.view', ['creator_id' => $contract->user_id])}}"> {{$contract->creator->name ?? tr('not_available')}}</a></td>
                            </tr>
                            <tr>
                                <td>{{tr('token_symbol')}}</td>
                                <td>{{$contract->symbol  ?: tr('not_available')}}</td>
                            </tr>
                            <tr>
                                <td>{{tr('total_supply')}}</td>
                                <td>{{$contract->total_supply ?: tr('not_available')}}</td>
                            </tr>
                            <tr>
                                <td>{{tr('wallet_address')}}</td>
                                <td>{{$contract->wallet_address ?: tr('not_available')}}</td>
                            </tr>
                            <tr>
                                <td>{{tr('contract_address')}}</td>
                                <td>{{$contract->contract_address  ?: tr('not_available')}}</td>
                            </tr>
                            <tr>
                                <td>{{tr('transaction_hash')}}</td>
                                <td>{{$contract->transaction_hash  ?: tr('not_available')}}</td>
                            </tr>
                            <tr>
                                <td>{{tr('pre_sale_start_date')}}</td>
                                <td>{{common_date($contract->pre_sale_start_date, Auth::guard('admin')->user()->timezone)}}</td>
                            </tr>
                            <tr>
                                <td>{{tr('public_sale_start_date')}}</td>
                                <td>{{common_date($contract->public_sale_start_date, Auth::guard('admin')->user()->timezone)}}</td>
                            </tr>
                            <tr>
                                <td>{{tr('is_presale_started')}}</td>
                                <td>
                                    @if($contract->is_presale_started == YES)

                                        <span class="btn btn-success btn-sm">{{ tr('yes') }}</span>

                                    @else

                                        <span class="btn btn-warning btn-sm">{{ tr('no') }}</span>

                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>{{tr('is_publicsale_started')}}</td>
                                <td>
                                    @if($contract->is_publicsale_started == YES)

                                        <span class="btn btn-success btn-sm">{{ tr('yes') }}</span>

                                    @else

                                        <span class="btn btn-warning btn-sm">{{ tr('no') }}</span>

                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>{{tr('description')}}</td>
                                <td>{{$contract->description  ?: tr('not_available')}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
         </div>
    </div>
    
    <div class="col-lg-6 col-12">
         
        <div class="box">
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover no-margin">
                        <tbody>
                            <tr>
                                <td>{{tr('pre_sale_mint_fee')}}</td>
                                <td>{{$contract->pre_sale_mint_fee ?: tr('not_available')}}</td>
                            </tr>
                            <tr>
                                <td>{{tr('public_sale_mint_fee')}}</td>
                                <td>{{$contract->public_sale_mint_fee ?: tr('not_available')}}</td>
                            </tr>
                            <!-- <tr>
                                <td>{{tr('total_nfts')}}</td>
                                <td>{{$contract->total_nfts ?: tr('not_available')}}</td>
                            </tr> -->
                            <tr>
                                <td>{{tr('total_pre_sale_nfts')}}</td>
                                <td>{{$contract->total_pre_sale_nfts ?: tr('not_available')}}</td>
                            </tr>
                            <tr>
                                <td>{{tr('total_public_sale_nfts')}}</td>
                                <td>{{$contract->total_public_sale_nfts ?: tr('not_available')}}</td>
                            </tr>
                            <tr>
                                <td>{{tr('total_nfts_generated')}}</td>
                                <td>
                                    <a href="{{route('admin.nfts.index' , ['contract_id' => $contract->id])}}">
                                        <span class="label label-success">{{$contract->total_nfts ?? tr('not_available')}}</span>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>{{tr('is_deployed')}}</td>
                                <td>
                                    @if($contract->is_deployed == YES)

                                        <span class="btn btn-success btn-sm">{{ tr('yes') }}</span>

                                    @else

                                        <span class="btn btn-warning btn-sm">{{ tr('no') }}</span>

                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>{{tr('deploye_access')}}</td>
                                <td>
                                    <span class="btn btn-{{ deploye_access_formatted($contract->deploye_access ?: DEPLOY_ACCESS_NONE)['class'] ?? tr('warning') }} btn-sm">{{ deploye_access_formatted($contract->deploye_access ?: DEPLOY_ACCESS_NONE)['message'] ?? tr('not_available') }}</span>
                                </td>
                            </tr>
                            <tr>
                                <td>{{tr('restrict_whitelisted_wallet')}}</td>
                                <td>
                                    @if($contract->restrict_whitelisted_wallet == YES)

                                        <span class="btn btn-success btn-sm">{{ tr('yes') }}</span>

                                    @else

                                        <span class="btn btn-warning btn-sm">{{ tr('no') }}</span>

                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>{{tr('status')}}</td>
                                <td>
                                    @if($contract->status == APPROVED)

                                        <span class="btn btn-success btn-sm">{{ tr('approved') }}</span>

                                    @else

                                        <span class="btn btn-warning btn-sm">{{ tr('declined') }}</span>

                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>{{tr('created_at')}}</td>
                                <td>{{common_date($contract->created_at, Auth::guard('admin')->user()->timezone)}}</td>
                            </tr>
                            <tr>
                                <td>{{tr('updated_at')}}</td>
                                <td>{{common_date($contract->updated_at, Auth::guard('admin')->user()->timezone)}}</td>
                            </tr>
                       </tbody>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</div>

@endsection
