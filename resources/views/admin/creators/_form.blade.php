<div class="row">

    <div class="col-lg-12 col-12">

        <div class="box">
            <div class="box-header with-border">
                
                <h3 class="box-title">{{ $creator->id ? tr('edit_creator') : tr('add_creator') }}</h3>

            </div>
                
            <form action="{{ (Setting::get('is_demo_control_enabled') == YES) ? '#' : route('admin.creators.save') }}" method="POST" enctype="multipart/form-data" role="form">
                
                @csrf

                <div class="box-body">

                    <div class="row">

                        <input type="hidden" name="creator_id" id="creator_id" value="{{ $creator->id}}">

                        <div class="col-md-6">
                                
                            <label for="name">{{ tr('name') }}*</label>

                            <div class="form-group">                                  
                                
                                <input type="text" id="name" name="name" class="form-control" placeholder="{{ tr('name') }}" value="{{old('name') ?: $creator->name}}" required onkeydown="return alphaOnly(event);">

                            </div>

                        </div>

                        <div class="col-md-6">
                                
                            <label for="email">{{ tr('email') }}*</label>

                            <div class="form-group">                                  
                                
                                <input type="text" id="email" name="email" class="form-control" placeholder="{{ tr('email') }}" value="{{old('email') ?: $creator->email}}" required>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <label>{{ tr('select_picture') }}</label>
                            <p class="text-muted mt-0 mb-0">{{tr('image_validate')}}</p>
                            <div class="form-group">

                                <input type="file" class="form-control"  id="picture" name="picture"  accept="image/png, image/jpg"  >
                            
                            </div>
                        </div>

                        @if(!$creator->id)

                            <div class="col-md-6">
                                <label>{{ tr('creator_applications') }}</label>
                                <p class="text-muted mt-0 mb-0">{{tr('creator_applications_para')}}</p>
                                <div class="form-group">
                                    <select class="form-control select2 w-p100" name="creator_application_id">

                                        <option value="0">Select Application</option>

                                        @foreach($creator_applications as $creator_application)
                                            <option value="{{$creator_application->id ?? "0"}}"
                                                @if(Request::has('creator_application_id') && Request::get('creator_application_id') == ($creator_application->id ?? "0")) 
                                                    selected="selected" 
                                                @endif
                                            >
                                                {{$creator_application->creator_name ?: ""}}
                                            </option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">  

                                <label for="password" class="">{{ tr('password') }} *</label>

                                <div class="form-group">
                                    <input type="password" minlength="6" required name="password" class="form-control" id="password" placeholder="{{ tr('password') }}" >
                                </div>

                            </div>

                            <div class="col-md-6">

                                <label for="confirm-password" class="">{{ tr('confirm_password') }} *</label>

                                <div class="form-group">
                                    <input type="password" minlength="6" required name="password_confirmation" class="form-control" id="confirm-password" placeholder="{{ tr('confirm_password') }}">
                                </div>
                            </div>


                        @endif

                    </div>

                </div>

                <div class="box-footer">
                    
                    <button type="reset" class="btn btn-warning btn-default btn-squared px-30">Reset</button>

                    <button type="submit" class="btn btn-info pull-right">Submit</button>

                </div>

            </form>

        </div>

    </div>

</div>