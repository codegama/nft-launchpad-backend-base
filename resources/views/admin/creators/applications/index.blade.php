@extends('layouts.admin')

@section('title', tr('creator_applications'))

@section('content-header', tr('creator_applications'))

@section('breadcrumb')

    <li class="breadcrumb-item active">
        <a href="{{route('admin.creators.applications.index')}}">{{ tr('creator_applications') }}</a>
    </li>

@endsection

@section('content')

<div class="row">   

    <div class="col-md-12">
         
        <div class="box">

            <div class="box-header with-border">
                <h3 class="box-title">{{tr('view_applications')}}</h3>
            </div>

            <!-- /.box-header -->
            <div class="box-body">

                <form method="GET" action="{{route('admin.creators.applications.index')}}">

                    <div class="row">

                        <div class="col-xs-12 col-sm-12 col-lg-3 col-md-3 offset-lg-3 offset-md-3 md-full-width resp-mrg-btm-md">

                        </div>

                        <div class="col-xs-12 col-sm-12 col-lg-6 col-md-12">

                            <div class="input-group">

                                <input type="text" class="form-control" name="search_key" value="{{Request::get('search_key')??''}}" placeholder="{{tr('creators_applications_search_placeholder')}}"> 

                                <span class="input-group-btn">
                                    &nbsp

                                    <button type="submit" class="btn btn-default reset-btn">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </button>

                                    <a href="{{route('admin.creators.applications.index')}}" class="btn btn-default reset-btn">
                                        <span> <i class="fa fa-eraser" aria-hidden="true"></i>
                                        </span>
                                    </a>

                                </span>

                            </div>

                        </div>

                    </div>

                </form>
                <br>

                <div class="table-responsive">
                    
                    <table id="example2" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">

                        <thead>
                            <tr>
                                <th>{{tr('s_no')}}</th>
                                <th>{{tr('creator_name')}}</th>
                                <th>{{tr('email')}}</th>
                                <th>{{tr('status')}}</th>
                                <th>{{tr('action')}}</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($creator_applications as $i => $creator_application)

                                <tr>

                                    <td>{{ $i+$creator_applications->firstItem() }}</td>

                                    <td class="white-space-nowrap">
                                        <a href="{{route('admin.creator.applications.view' , ['creator_application_id' => $creator_application->id])}}" class="custom-a">
                                            {{$creator_application->creator_name ?: tr('not_available')}}
                                        </a>
                                        
                                    </td>

                                    <td>
                                        {{ $creator_application->email ?: tr('not_available') }}
                                    </td>

                                    <td>
                                        @if($creator_application->user_id)

                                            <span class="label label-success">{{ tr('completed') }}</span>

                                        @else

                                            <span class="label label-warning">{{ tr('pending') }}</span>

                                        @endif
                                    </td>
                                   
                                    <td>

                                        <div class="btn-group" role="group">

                                            <button class="btn btn-primary dropdown-toggle dropdown-menu-right" id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ft-settings icon-left"></i> {{ tr('action') }}</button>

                                            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">

                                                <a class="dropdown-item" href="{{ route('admin.creator.applications.view', ['creator_application_id' => $creator_application->id] ) }}">&nbsp;{{ tr('view') }}</a>


                                                @if(Setting::get('is_demo_control_enabled') == YES)

                                                    <a class="dropdown-item" href="javascript:void(0)">&nbsp;{{ tr('delete') }}</a>

                                                @else

                                                    <a class="dropdown-item" onclick="return confirm(&quot;{{ tr('creator_application_delete_confirmation' , $creator_application->creator_name) }}&quot;);" href="{{ route('admin.creator.applications.delete', ['creator_application_id' => $creator_application->id,'page'=>request()->input('page')] ) }}">&nbsp;{{ tr('delete') }}</a>

                                                @endif

                                                @if(!$creator_application->user_id)

                                                    <a class="dropdown-item" href="{{ route('admin.creators.create', ['creator_application_id' => $creator_application->id] ) }}">&nbsp;{{ tr('create_creator') }}</a>

                                                @endif
                                                
                                            </div>

                                        </div>

                                    </td>

                                </tr>
                                
                            @endforeach                         
                        </tbody>                       
                    </table>
                
                </div>

            </div>

            <div class="box-footer clearfix">
                    
                <div class="pull-right rd-flex">
                    {{$creator_applications->appends(request()->input())->links('pagination::bootstrap-4')}}
                </div>
            </div>

        </div>
    </div>
</div>

@endsection


@section('scripts')
