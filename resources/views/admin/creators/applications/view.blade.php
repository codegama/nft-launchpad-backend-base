@extends('layouts.admin')

@section('title', tr('creator_applications'))

@section('content-header', tr('creator_applications'))

@section('breadcrumb')

    <li class="breadcrumb-item active">
        <a href="{{route('admin.creators.applications.index')}}">{{ tr('creator_applications') }}</a>
    </li>

    <li class="breadcrumb-item">{{tr('view_creator_application')}}</li>

@endsection

@section('content')

<div class="box">
    
    <div class="box-body">

    	<div class="row">
			
			<div class="col-md-12">
                
		    	<div class="media-list media-list-divided">

					<div class="media media-single">

					  	<img class="w-80" src="{{$creator_application->artwork_picture}}" alt="artwork_picture">
					  	<div class="media-body">
							<h6>{{$creator_application->creator_name}}</h6>
							<small class="text-fader">{{common_date($creator_application->created_at , Auth::guard('admin')->user()->timezone)}}</small>
					  	</div>

					  	@if(Setting::get('is_demo_control_enabled') == YES)

	                        <div class="media-right">
								<button class="btn bg-purple margin"><i class="fa fa-edit"></i> {{tr('edit')}}</button>
					  		</div>

	                        <div class="media-right">
								<button class="btn bg-olive margin"><i class="fa fa-trash"></i> {{tr('delete')}}</button>
					  		</div>

                        @else

	                        <div class="media-right">

	                        	<a class="btn bg-olive margin" onclick="return confirm(&quot;{{ tr('creator_application_delete_confirmation' , $creator_application->creator_name) }}&quot;);" href="{{ route('admin.creator.applications.delete', ['creator_application_id' => $creator_application->id] ) }}"><i class="fa fa-trash"></i> {{tr('delete')}}</a>
	                        </div>

                        @endif

                        @if(!$creator_application->user_id)

                            <div class="media-right">
                                <a class="btn bg-purple margin" href="{{ route('admin.creators.create', ['creator_application_id' => $creator_application->id] ) }}"><i class="fa fa-users"></i> {{tr('create_creator')}}</a>
                            </div>

                        @endif

 					</div>
				</div>
			</div>
		</div>
    </div>
</div>      

<div class="row">
	
	<div class="col-md-12">
		
		<div class="box">
			
			<div class="box-body">

                <div class="row">

                    <div class="col-md-12 pb-15">

                        <div class="table-responsive">
                            <table class="table table-striped table-hover no-margin">
                                <tbody>
                                    <tr>
                                        <th>{{tr('agree_terms')}}</th>
                                        <td>
                                            @if($creator_application->agree_terms)

                                                <span class="btn btn-success btn-sm">{{ tr('agreed') }}</span>

                                            @else

                                                <span class="btn btn-warning btn-sm">{{ tr('disagree') }}</span>

                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>{{tr('mint_time_period')}}</th>
                                        <td>{{$creator_application->mint_time_period ?: tr('n_a')}} {{$creator_application->mint_time_period > 1 ? "Days" : "Day"}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{tr('is_team_dox')}}</th>
                                        <td class="text-capitalize">
                                            @if($creator_application->is_team_dox)

                                                <span class="btn btn-success btn-sm">{{ tr('yes') }}</span>

                                            @else

                                                <span class="btn btn-warning btn-sm">{{ tr('no') }}</span>

                                            @endif
                                        </td>
                                    </tr>

                                </tbody>
                            </table>

                        </div>
                    </div>

                    <div class="col-md-6">

                        <div class="table-responsive">
        					<table class="table table-striped table-hover no-margin">
                                <tbody>
                                 	<tr>
                                    	<th>{{tr('creator_name')}}</th>
                                    	<td>{{$creator_application->creator_name ?: tr('n_a')}}</td>
                                  	</tr>
                                    <tr>
                                        <th>{{tr('email')}}</th>
                                        <td>{{$creator_application->email ?: tr('n_a')}}</td>
                                    </tr>
                                  	<tr>
                                        <th>{{tr('twitter_followers')}}</th>
                                        <td class="text-capitalize">{{$creator_application->twitter_followers ?: tr('n_a')}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{tr('project_artwork')}}</th>
                                        <td class="text-capitalize">{{$creator_application->project_artwork ?: tr('n_a')}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{tr('mint_date')}}</th>
                                        <td class="text-capitalize">{{$creator_application->mint_date ?: tr('n_a')}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{tr('items_count')}}</th>
                                        <td class="text-capitalize">{{$creator_application->items_count ?: tr('n_a')}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{tr('mint_amount')}}</th>
                                        <td class="text-capitalize">{{$creator_application->mint_amount ?: tr('n_a')}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{tr('is_collection_derivative')}}</th>
                                        <td>
                                            @if($creator_application->is_collection_derivative)

                                                <span class="btn btn-success btn-sm">{{ tr('yes') }}</span>

                                            @else

                                                <span class="btn btn-warning btn-sm">{{ tr('no') }}</span>

                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{{tr('status')}}</td>
                                        <td>
                                            @if($creator_application->user_id)

                                                <span class="btn btn-success btn-sm">{{ tr('completed') }}</span>

                                            @else

                                                <span class="btn btn-warning btn-sm">{{ tr('pending') }}</span>

                                            @endif
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>

                    <div class="col-md-6">

                        <div class="table-responsive">
                            <table class="table table-striped table-hover no-margin">
                                <tbody>
                                    <tr>
                                        <th>{{tr('discord_id')}}</th>
                                        <td class="text-capitalize">{{$creator_application->discord_id ?: tr('n_a')}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{tr('twitter_link')}}</th>
                                        <td class="text-capitalize">{{$creator_application->twitter_link ?: tr('n_a')}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{tr('discord_link')}}</th>
                                        <td class="text-capitalize">{{$creator_application->discord_link ?: tr('n_a')}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{tr('instagram_link')}}</th>
                                        <td class="text-capitalize">{{$creator_application->instagram_link ?: tr('n_a')}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{tr('website_link')}}</th>
                                        <td class="text-capitalize">{{$creator_application->website_link ?: tr('n_a')}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{tr('external_link')}}</th>
                                        <td class="text-capitalize">{{$creator_application->external_link ?: tr('n_a')}}</td>
                                    </tr>
                                    <tr>
                                        <td>{{tr('created_at')}}</td>
                                        <td>{{common_date($creator_application->created_at, Auth::guard('admin')->user()->timezone)}}</td>
                                    </tr>
                                    <tr>
                                        <td>{{tr('updated_at')}}</td>
                                        <td>{{common_date($creator_application->updated_at, Auth::guard('admin')->user()->timezone)}}</td>
                                    </tr>

                               </tbody>
                            </table>
                        
                        </div>

                    </div>
				</div>
			</div>
			<!-- /.box-body -->
		 </div>
	</div>
</div>

<div class="row">
    
    <div class="col-md-12">
        
        <div class="box">
            
            <div class="box-body">

                <div class="row">

                    <div class="col-md-12">

                        <div class="table-responsive">
                            <table class="table table-striped table-hover no-margin">
                                <tbody>
                                    <tr>
                                        <th>{{tr('project_description')}}</th>
                                        <td class="text-capitalize">{{$creator_application->project_description ?: tr('n_a')}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{tr('project_long_term_goal')}}</th>
                                        <td class="text-capitalize">{{$creator_application->project_long_term_goal ?: tr('n_a')}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{tr('about_team')}}</th>
                                        <td class="text-capitalize">{{$creator_application->about_team ?: tr('n_a')}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{tr('team_link_attachment')}}</th>
                                        <td class="text-capitalize">{{$creator_application->team_link_attachment ?: tr('n_a')}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{tr('partnerships')}}</th>
                                        <td class="text-capitalize">{{$creator_application->partnerships ?: tr('n_a')}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{tr('partnerships_link_attachment')}}</th>
                                        <td class="text-capitalize">{{$creator_application->partnerships_link_attachment ?: tr('n_a')}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{tr('investment')}}</th>
                                        <td class="text-capitalize">{{$creator_application->investment ?: tr('n_a')}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{tr('investment_link_attachment')}}</th>
                                        <td class="text-capitalize">{{$creator_application->investment_link_attachment ?: tr('n_a')}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{tr('whitepaper_roadmap')}}</th>
                                        <td class="text-capitalize">{{$creator_application->whitepaper_roadmap ?: tr('n_a')}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{tr('whitepaper_roadmap_link_attachment')}}</th>
                                        <td class="text-capitalize">{{$creator_application->whitepaper_roadmap_link_attachment ?: tr('n_a')}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{tr('artwork')}}</th>
                                        <td class="text-capitalize">{{$creator_application->artwork ?: tr('n_a')}}</td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
         </div>
    </div>
</div>

@endsection
