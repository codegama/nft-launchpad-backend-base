@extends('layouts.admin')

@section('title', tr('creators'))

@section('content-header', tr('creators'))

@section('breadcrumb')

    <li class="breadcrumb-item"><a href="{{route('admin.creators.index')}}">{{tr('creators')}}</a></li>
    
    <li class="breadcrumb-item active">{{tr('edit_creator')}}</a></li>

@endsection

@section('content')

    @include('admin.creators._form')

@endsection
