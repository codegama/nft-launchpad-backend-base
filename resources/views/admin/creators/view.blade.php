@extends('layouts.admin')

@section('title', tr('creators'))

@section('content-header', tr('creators'))

@section('breadcrumb')

    <li class="breadcrumb-item active">
        <a href="{{route('admin.creators.index')}}">{{ tr('creators') }}</a>
    </li>

    <li class="breadcrumb-item">{{tr('view_creators')}}</li>

@endsection

@section('content')

<div class="box">
    
    <div class="box-body">

    	<div class="row">
			
			<div class="col-md-12">
                
		    	<div class="media-list media-list-divided">

					<div class="media media-single">

					  	<img class="w-80" src="{{$creator->picture}}" alt="...">
					  	<div class="media-body">
							<h6>{{$creator->name}}</h6>
							<small class="text-fader">{{common_date($creator->created_at , Auth::guard('admin')->user()->timezone)}}</small>
					  	</div>

					  	@if($creator->status == APPROVED)

					  		<div class="media-right">

                            	<a class="btn bg-navy margin" href="{{  route('admin.creators.status' , ['creator_id' => $creator->id] )  }}" onclick="return confirm(&quot;{{ $creator->name }} - {{ tr('creator_decline_confirmation') }}&quot;);"> <i class="fa fa-check"></i>  {{ tr('decline') }}
                                                </a>
                            </div>

                        @else

                        	<div class="media-right">

                            	<a class="btn bg-navy margin" href="{{ route('admin.creators.status' , ['creator_id' => $creator->id] ) }}"> <i class="fa fa-check"></i>  {{ tr('approve') }}</a>
                            </div>

                        @endif

					  	@if(Setting::get('is_demo_control_enabled') == YES)

	                        <div class="media-right">
								<button class="btn bg-purple margin"><i class="fa fa-edit"></i> {{tr('edit')}}</button>
					  		</div>

	                        <div class="media-right">
								<button class="btn bg-olive margin"><i class="fa fa-trash"></i> {{tr('delete')}}</button>
					  		</div>

                        @else

                        	<div class="media-right">

	                        	<a class="btn bg-purple margin" href="{{ route('admin.creators.edit', ['creator_id' => $creator->id] ) }}"><i class="fa fa-edit"></i> {{tr('edit')}}</a>
	                        </div>	

	                        <div class="media-right">

	                        	<a class="btn bg-olive margin" onclick="return confirm(&quot;{{ tr('creator_delete_confirmation' , $creator->name) }}&quot;);" href="{{ route('admin.creators.delete', ['creator_id' => $creator->id] ) }}"><i class="fa fa-trash"></i> {{tr('delete')}}</a>
	                        </div>

                        @endif

                        <div class="media-right">

                            <a class="btn bg-warning margin" href="{{ route('admin.contracts.index', ['creator_id' => $creator->id] ) }}"><i class="fa fa-eye"></i> {{tr('contracts')}}</a>
                        
                        </div>


 					</div>
				</div>
			</div>
		</div>
    </div>
</div>      

<div class="row">
	
	<div class="col-md-12">
		
		<div class="box">
			
			<div class="box-body">

                <div class="row">

                    <div class="col-md-6">

                        <div class="table-responsive">
        					<table class="table table-striped table-hover no-margin">
                                <tbody>
                                 	<tr>
                                    	<th>{{tr('name')}}</th>
                                    	<td>{{$creator->name ?: tr('n_a')}}</td>
                                  	</tr>
                                    <tr>
                                        <th>{{tr('email')}}</th>
                                        <td>{{$creator->email ?: tr('n_a')}}</td>
                                    </tr>
                                  	<!-- <tr>
                                    	<th>{{tr('wallet_address')}}</th>
                                    	<td>{{$creator->wallet_address ?: tr('n_a')}}</td>
                                  	</tr> -->
                                  	<tr>
                                        <th>{{tr('timezone')}}</th>
                                        <td class="text-capitalize">{{$creator->timezone ?: tr('not_available')}}</td>
                                    </tr>

                                </tbody>
                            </table>

                        </div>
                    </div>

                    <div class="col-md-6">

                        <div class="table-responsive">
                            <table class="table table-striped table-hover no-margin">
                                <tbody>
                                    <tr>
                                        <td>{{tr('status')}}</td>
                                        <td>
                                            @if($creator->status == APPROVED)

                                                <span class="btn btn-success btn-sm">{{ tr('approved') }}</span>

                                            @else

                                                <span class="btn btn-warning btn-sm">{{ tr('declined') }}</span>

                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{{tr('created_at')}}</td>
                                        <td>{{common_date($creator->created_at, Auth::guard('admin')->user()->timezone)}}</td>
                                    </tr>
                                    <tr>
                                        <td>{{tr('updated_at')}}</td>
                                        <td>{{common_date($creator->updated_at, Auth::guard('admin')->user()->timezone)}}</td>
                                    </tr>

                               </tbody>
                            </table>
                        
                        </div>
                        <!-- <div class="row">
                            
                            <div class="media-list media-list-divided">

                                <div class="media media-single">
                                    <div class="media-right">

                                        <a class="btn bg-warning margin" href="{{ route('admin.contracts.index', ['creator_id' => $creator->id] ) }}"><i class="fa fa-eye"></i> {{tr('contracts')}}</a>
                                    
                                    </div>
                                </div>
                            </div>
                        </div> -->

                    </div>
				</div>
			</div>
			<!-- /.box-body -->
		 </div>
	</div>
</div>

@endsection
