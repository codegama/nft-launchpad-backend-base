@extends('layouts.admin')

@section('title', tr('nfts'))

@section('content-header', tr('nfts'))

@section('breadcrumb')

    <li class="breadcrumb-item active">
        <a href="{{route('admin.nfts.index')}}">{{ tr('nfts') }}</a>
    </li>

    <li class="breadcrumb-item">{{tr('view_nfts')}}</li>

@endsection

@section('content')

<div class="row">
    <div class="col-md-12">

        <div class="box">

            <div class="box-header with-border">
                
                <h3 class="box-title">{{tr('view_nfts')}}
                    @if(Request::has('contract_id') && Request::get('contract_id') != "")
                    - <a href="{{route('admin.contracts.view',['contract_id'=>Request::get('contract_id') ?? '', ])}}">
                        {{$contract->contract_name ?: tr('n_a')}}
                    </a>
                    @endif
                </h3>

            </div>

            <div class="box-body">

                <!-- <div class="callout bg-pale-secondary">
                    <h4>Notes:</h4>
                    <p>
                        <ul>
                            <li>All the projects added by the project owner will be displayed here with basic information. </li>
                        </ul>
                    </p>
                </div> -->

                <form method="GET" action="{{route('admin.nfts.index')}}">

                    <div class="row">

                        <input type="hidden" id="contract_id" name="contract_id" value="{{Request::get('contract_id') ?? ''}}">

                        <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 resp-mrg-btm-md">
                            @if(Request::has('search_key'))
                            <p class="text-muted">Search results for: <b>{{Request::get('search_key')}}</b></p>
                            @endif
                        </div>

                        <div class="col-xs-12 col-sm-12 col-lg-3 col-md-3 offset-lg-3 offset-md-3 md-full-width resp-mrg-btm-md">

                            <select class="form-control select2" name="mint_status">

                                <option class="select-color" value="">{{tr('select_mint_status')}}</option>

                                <option class="select-color" value="{{MINT_PENDING}}" @if(Request::get('mint_status') == MINT_PENDING && Request::get('mint_status')!='' ) selected @endif>{{tr('pending')}}</option>

                                <option class="select-color" value="{{MINT_INITIATED}}" @if(Request::get('mint_status') == MINT_INITIATED && Request::get('mint_status')!='' ) selected @endif>{{tr('initiated')}}</option>

                                <option class="select-color" value="{{MINT_COMPLETED}}" @if(Request::get('mint_status') == MINT_COMPLETED && Request::get('mint_status')!='' ) selected @endif>{{tr('completed')}}</option>

                            </select>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-lg-6 col-md-12">

                            <div class="input-group">

                                <input type="text" class="form-control" name="search_key" value="{{Request::get('search_key')??''}}" placeholder="{{tr('nfts_search_placeholder')}}"> 

                                <span class="input-group-btn">
                                    &nbsp

                                    <button type="submit" class="btn btn-default reset-btn">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </button>
                                    @if(Request::get('contract_id'))
                                    <a href="{{route('admin.nfts.index',['contract_id'=>Request::get('contract_id') ?? '', ])}}" class="btn btn-default reset-btn">
                                        <span> <i class="fa fa-eraser" aria-hidden="true"></i>
                                        </span>
                                    </a>
                                    @else
                                    <a href="{{route('admin.nfts.index')}}" class="btn btn-default reset-btn">
                                        <span> <i class="fa fa-eraser" aria-hidden="true"></i>
                                        </span>
                                    </a>
                                    @endif
                                </span>

                            </div>

                        </div>

                    </div>

                </form>
                <br>

                <div class="table-responsive">
                    
                    <table id="example2" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                        
                        <thead>
                            
                            <tr>
                                <th>{{ tr('s_no') }}</th>
                                <th>{{ tr('contract_name') }}</th>
                                <th>{{ tr('creator_name') }}</th>
                                <th>{{ tr('file_name') }}</th>
                                <!-- <th>{{ tr('picture') }}</th> -->
                                <!-- <th>{{ tr('mint_type') }}</th> -->
                                <th>{{ tr('mint_status') }}</th>
                                <th>{{ tr('action') }}</th>
                            </tr>

                        </thead>
                        <tbody>
                            
                            @foreach($nfts as $i => $nft)

                                <tr>

                                    <td>{{ $i+$nfts->firstItem() }}</td>

                                    <td>
                                        <small><a href="{{route('admin.contracts.view' , ['contract_id' => $nft->contract_id])}}">{{$nft->contract->contract_name ?? tr('not_available')}}</a></small>
                                    </td>

                                    <td>
                                        <small><a href="{{route('admin.creators.view' , ['creator_id' => ($nft->contract->creator->id ?? 0)])}}">{{$nft->contract->creator->name ?? tr('not_available')}}</a></small>
                                    </td>

                                    <td>

                                        <a href="{{route('admin.nfts.view' , ['merge_image_id' => $nft->id])}}">
                                            {{$nft->file_name ?: tr('not_available')}}
                                        </a>

                                    </td>

                                    <!-- <td>

                                        <a href="{{route('admin.nfts.view' , ['merge_image_id' => $nft->id])}}">
                                            <img src="{{$nft->picture ?: asset('artwork.png')}}" style="width: 15%">
                                        </a>

                                    </td>
 -->
                                    <!-- <td>
                                        {{$nft->mint_type ?: tr('n_a')}}
                                    </td> -->

                                    <td>
                                        @if($nft->mint_status == MINT_PENDING)

                                            <span class="label label-warning">{{ tr('pending') }}</span>

                                        @elseif($nft->mint_status == MINT_COMPLETED)

                                            <span class="label label-success">{{ tr('completed') }}</span>

                                        @else

                                            <span class="label label-primary">{{ tr('initiated') }}</span>

                                        @endif
                                    </td>
                                   
                                    <td>

                                        <div class="btn-group" role="group">

                                            <button type="button" class="btn btn-primary dropdown-toggle dropdown-menu-right" id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ft-settings icon-left"></i> {{ tr('action') }}</button>

                                            <div class="dropdown-menu action-dropdown-menu" aria-labelledby="btnGroupDrop1">

                                                <a class="dropdown-item" href="{{route('admin.nfts.view' , ['merge_image_id' => $nft->id])}}">&nbsp;{{ tr('view') }}</a>


                                                @if(Setting::get('is_demo_control_enabled') == YES)

                                                <a class="dropdown-item" href="javascript:void(0)">&nbsp;{{ tr('delete') }}</a>

                                                @else

                                                <a class="dropdown-item" onclick="return confirm(&quot;{{ tr('nft_delete_confirmation' , $nft->file_name) }}&quot;);" href="{{ route('admin.nfts.delete', ['merge_image_id' => $nft->id,'page'=>request()->input('page')] ) }}">&nbsp;{{ tr('delete') }}</a>

                                                @endif

                                            </div>

                                        </div>

                                    </td>

                                </tr>
                                
                                @endforeach

                        </tbody>
                    </table>
                </div>
            </div>

            <div class="box-footer clearfix">
                    
                <div class="pull-right rd-flex">{{ $nfts->appends(request()->input())->links('pagination::bootstrap-4') }}</div>
            </div>

        </div>

    </div>
</div>


@endsection