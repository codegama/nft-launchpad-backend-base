@extends('layouts.admin')

@section('title', tr('nfts'))

@section('content-header', tr('nft'))

@section('breadcrumb')

    <li class="breadcrumb-item active">
        <a href="{{route('admin.nfts.index')}}">{{ tr('nfts') }}</a>
    </li>

    <li class="breadcrumb-item">{{tr('view_nft')}}</li>

@endsection

@section('content')

<!-- <div class="box">
    
    <div class="box-body">

        <div class="row">
            
            <div class="col-md-12 d-flex">
                <div class="col-md-5">
                    <div class="media-list media-list-divided">

                        <div class="media media-single">

                            <img class="w-80 border-2" src="{{$nft->picture}}" alt="...">
                            
                            <div class="media-body">
                                <h6>{{$nft->file_name ?: tr('n_a')}}</h6>
                                <small class="text-fader">{{common_date($nft->created_at , Auth::guard('admin')->user()->timezone)}}</small>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="media-list media-list-divided">

                        @if(Setting::get('is_demo_control_enabled') == YES)

                            <button class="btn bg-olive margin"><i class="fa fa-trash"></i> {{tr('delete')}}</button>

                        @else

                            <a class="btn bg-warning margin" onclick="return confirm(&quot;{{ tr('nft_delete_confirmation' , $nft->file_name) }}&quot;);" href="{{ route('admin.nfts.delete', ['merge_image_id' => $nft->id] ) }}"><i class="fa fa-trash"></i> {{tr('delete')}}</a>

                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->

<div class="row">
    
    <div class="col-lg-6 col-12">
        
        <div class="box">
            
            <div class="box-body">
                <div>
                    <img class="border-2" src="{{$nft->picture}}" alt="...">
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-hover no-margin">
                        <tbody>
                            <tr>
                                <td>{{tr('unique_id')}}</td>
                                <td>{{$nft->unique_id ?: tr('not_available')}}</td>
                            </tr>
                            <tr>
                                <td>{{tr('creator')}}</td>
                                <td><a href="{{route('admin.creators.view', ['creator_id' => $contract->user_id])}}"> {{$contract->creator->name ?? tr('not_available')}}</a></td>
                            </tr>
                            <tr>
                                <td>{{tr('contract')}}</td>
                                <td><a href="{{route('admin.contracts.view', ['contract_id' => $contract->id])}}"> {{$contract->contract_name ?: tr('not_available')}}</a></td>
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
         </div>
    </div>
    
    <div class="col-lg-6 col-12">
         
        <div class="box">
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover no-margin">
                        <tbody>
                            <tr>
                                <td>{{tr('file_name')}}</td>
                                <td>{{$nft->file_name ?: tr('not_available')}}</td>
                            </tr>
                            <tr>
                                <td>{{tr('minted_wallet_address')}}</td>
                                <td>
                                    {{$nft->minted_wallet_address ?: tr('n_a')}}
                                </td>
                            </tr>
                            <tr>
                                <td>{{tr('mint_type')}}</td>
                                <td>
                                    {{$nft->mint_type ?: tr('n_a')}}
                                </td>
                            </tr>

                            <tr>
                                <td>{{tr('mint_status')}}</td>
                                <td>
                                    @if($nft->mint_status == MINT_PENDING)

                                        <span class="label label-warning">{{ tr('pending') }}</span>

                                    @elseif($nft->mint_status == MINT_COMPLETED)

                                        <span class="label label-success">{{ tr('completed') }}</span>

                                    @else

                                        <span class="label label-primary">{{ tr('initiated') }}</span>

                                    @endif
                                </td>
                            </tr>
                            @if($properties->count() > 0)
                                <tr>
                                    <td><h5>{{tr('properties')}}:</h5></td><td></td>
                                </tr>
                                @foreach($properties as $property)
                                <tr>
                                    <td>{{$property->name ?: tr('n_a')}}</td>
                                    <td>{{$property->values->value ?? tr('n_a')}}</td>
                                </tr>
                                @endforeach
                            @endif
                            <tr>
                                <td>{{tr('created_at')}}</td>
                                <td>{{common_date($nft->created_at, Auth::guard('admin')->user()->timezone)}}</td>
                            </tr>
                            <tr>
                                <td>{{tr('updated_at')}}</td>
                                <td>{{common_date($nft->updated_at, Auth::guard('admin')->user()->timezone)}}</td>
                            </tr>
                       </tbody>
                    </table>
                </div>
                <div class="row">
                            
                    <div class="media-list media-list-divided">

                        <div class="media media-single">
                            <div class="media-right">

                                <a class="btn bg-warning margin" onclick="return confirm(&quot;{{ tr('nft_delete_confirmation' , $nft->file_name) }}&quot;);" href="{{ route('admin.nfts.delete', ['merge_image_id' => $nft->id] ) }}"><i class="fa fa-trash"></i> {{tr('delete')}}</a>
                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</div>

@endsection
