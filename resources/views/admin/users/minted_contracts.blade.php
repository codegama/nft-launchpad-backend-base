@extends('layouts.admin')

@section('title', tr('minted_contracts'))

@section('content-header', tr('minted_contracts'))

@section('breadcrumb')

    <li class="breadcrumb-item active">
        <a href="{{route('admin.minted_contracts')}}">{{ tr('minted_contracts') }}</a>
    </li>

@endsection

@section('content')

<div class="row">   

    <div class="col-md-12">
         
        <div class="box">

            <div class="box-header with-border">
                <h3 class="box-title">{{tr('view_minted_contracts')}}
                    @if(Request::has('user_id') && Request::get('user_id') != "")
                    -
                        <a href="{{route('admin.users.view' , ['user_id' => $user->id])}}" class="custom-a">
                            {{$user->name ?: tr('not_available')}}
                        </a>
                    @endif
                </h3>
            </div>

            <!-- /.box-header -->
            <div class="box-body">

                <form method="GET" action="{{route('admin.minted_contracts')}}">

                    <div class="row">

                        <input type="hidden" class="form-control" name="user_id" value="{{Request::get('user_id')??''}}">

                        <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 resp-mrg-btm-md">
                            @if(Request::has('search_key'))
                            <p class="text-muted">Search results for <b>{{Request::get('search_key')}}</b></p>
                            @endif
                        </div>

                        <div class="col-xs-12 col-sm-12 col-lg-3 col-md-3 offset-lg-3 offset-md-3 md-full-width resp-mrg-btm-md">

                        </div>

                        <div class="col-xs-12 col-sm-12 col-lg-6 col-md-12">

                            <div class="input-group">

                                <input type="text" class="form-control" name="search_key" value="{{Request::get('search_key')??''}}" placeholder="{{tr('minted_contracts_search_placeholder')}}"> 

                                <span class="input-group-btn">
                                    &nbsp

                                    <button type="submit" class="btn btn-default reset-btn">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </button>

                                    <a href="{{route('admin.minted_contracts', ['user_id' => Request::get('user_id')??'' ])}}" class="btn btn-default reset-btn">
                                        <span> <i class="fa fa-eraser" aria-hidden="true"></i>
                                        </span>
                                    </a>

                                </span>

                            </div>

                        </div>

                    </div>

                </form>
                <br>

                <div class="table-responsive">
                    
                    <table id="example2" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">

                        <thead>
                            <tr>
                                <th>{{tr('s_no')}}</th>
                                <th>{{tr('contract_name')}}</th>
                                <th>{{tr('total_items_minted')}}</th>
                                <th>{{tr('status')}}</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($minted_contracts as $i => $minted_contract)

                                <tr>

                                    <td>{{ $i+$minted_contracts->firstItem() }}</td>

                                    <td class="white-space-nowrap">
                                        <a class="text-yellow hover-warning" href="{{route('admin.contracts.view' , ['contract_id' => $minted_contract->id])}}">
                                            {{$minted_contract->contract_name ?: tr('not_available')}}
                                        </a>
                                        
                                    </td>

                                    <td>
                                        <a href="{{route('admin.nfts.index' , ['minted_user_id' => $user->id ?? 0, 'contract_id' => $minted_contract->id ?? 0])}}">
                                            <span class="label label-success">
                                                {{ $minted_contract->minted_items_count ?? tr('not_available') }}
                                            </span>
                                        </a>
                                    </td>

                                    <td>
                                        
                                        <span class="label label-success">{{ tr('minted') }}</span>

                                    </td>

                                </tr>
                                
                            @endforeach                         
                        </tbody>                       
                    </table>
                
                </div>

            </div>

            <div class="box-footer clearfix">
                    
                <div class="pull-right rd-flex">
                    {{$minted_contracts->appends(request()->input())->links('pagination::bootstrap-4')}}
                </div>
            </div>

        </div>
    </div>
</div>

@endsection

