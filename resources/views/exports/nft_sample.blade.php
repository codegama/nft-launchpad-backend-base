<!DOCTYPE html>
<html>

<head>
    <title>{{tr('users')}}</title>
</head>
<style type="text/css">

    table{
        font-family: arial, sans-serif;
        border-collapse: collapse;
    }

    .first_row_design{
        background-color: #187d7d;
        color: #ffffff;
    }

    .row_col_design{
        background-color: #cccccc;
    }

    th{
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
        font-weight: bold;

    }

    td {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;

    }
    
</style>

<body>

    <table>

        <!-- HEADER START  -->

        <tr class="first_row_design">

            @foreach($data['properties'] as $i => $property)

                <th>{{$property->name}}</th>

            @endforeach

            <th>{{tr('name')}}</th>

        </tr>

        <!--- HEADER END  -->

        @for ($j = 0; $j <= 4; $j++)

            <tr @if($j % 2 == 0) class="row_col_design" @endif>

                @foreach($data['properties'] as $i => $property)

                    <td>{{$data['property_values'][array_rand($data['property_values'])]}}</td>

                @endforeach

                <td>00{{$j + 1}}.png</td>
              
            </tr>

        @endfor

        
    </table>

</body>

</html>