<?php

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Route;


Route::get('get_settings_json', function () {

    if(\File::isDirectory(public_path(SETTINGS_JSON))){

    } else {

        \File::makeDirectory(public_path('default-json'), 0777, true, true);

        \App\Helpers\Helper::settings_generate_json();
    }

    $jsonString = file_get_contents(public_path(SETTINGS_JSON));

    $data = json_decode($jsonString, true);

    return $data;

});

Route::post('login','Api\Creator\AccountApiController@login');

Route::post('refresh_token_update','Api\Creator\AccountApiController@refresh_token_update');

Route::post('register_application_save','Api\Creator\AccountApiController@register_application_save');

Route::group(['middleware' => 'CreatorApiVal'] , function() {

    Route::post('profile','Api\Creator\AccountApiController@profile');

    Route::post('logout', 'Api\Creator\AccountApiController@logout');

    Route::post('update_profile', 'Api\Creator\AccountApiController@update_profile');

    Route::post('change_password', 'Api\Creator\AccountApiController@change_password');

    Route::post('delete_account', 'Api\Creator\AccountApiController@delete_account');

    Route::post('dashboard','Api\Creator\ContractApiController@dashboard');

    //Countract CRUD

    Route::post('contracts_save','Api\Creator\ContractApiController@contracts_save');

    Route::post('contracts_list','Api\Creator\ContractApiController@contracts_list');

    Route::post('contracts_view','Api\Creator\ContractApiController@contracts_view');

    Route::post('contracts_delete','Api\Creator\ContractApiController@contracts_delete');

    Route::post('update_deploy_access','Api\Creator\ContractApiController@update_deploy_access');

    Route::post('deploy_contract_save','Api\Creator\ContractApiController@deploy_contract_save');

    Route::post('whitelisted_wallet_list','Api\Creator\ContractApiController@whitelisted_wallet_list');

    Route::post('whitelisted_wallet_save','Api\Creator\ContractApiController@whitelisted_wallet_save');

    Route::post('set_mint_fee_update','Api\Creator\ContractApiController@set_mint_fee_update');

    //NFt Properties CRUD

    Route::post('nft_properties_save', 'Api\Creator\NftApiController@nft_properties_save');

    Route::post('nft_properties_list', 'Api\Creator\NftApiController@nft_properties_list');

    Route::post('nft_properties_delete', 'Api\Creator\NftApiController@nft_properties_delete');

    Route::post('export_sample_nft', 'Api\Creator\NftApiController@export_sample_nft');

    //NFt Images CRUD

    Route::post('nft_merge_excel_import_save', 'Api\Creator\NftApiController@nft_merge_excel_import_save');

    Route::post('nft_merge_images_list', 'Api\Creator\NftApiController@nft_merge_images_list');

    Route::post('nft_merge_image_view', 'Api\Creator\NftApiController@nft_merge_image_view');

    Route::post('nft_merge_image_delete', 'Api\Creator\NftApiController@nft_merge_image_delete');

    Route::post('nft_mint_status_update', 'Api\Creator\NftApiController@nft_mint_status_update');

    Route::post('nft_single_merge_images_save', 'Api\Creator\NftApiController@nft_single_merge_images_save');

});

